package jdp.singleton.v3;

public class MySingleton {
    private static volatile MySingleton instance;
    private MySingleton() {

    }
    /* Only block the critical part. */
    public static MySingleton getInstance() {
        if (instance == null) {
            synchronized (MySingleton.class) {
                if (instance == null)   // thread-safe query
                    instance = new MySingleton();
            }
        }
        return instance;
    }
}
