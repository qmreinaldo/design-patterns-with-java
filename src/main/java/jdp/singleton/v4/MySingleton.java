package jdp.singleton.v4;

public class MySingleton {
    private static final MySingleton INSTANCE = new MySingleton();  // Early loading.

    private MySingleton() {

    }

    public static MySingleton getInstance() {
        return INSTANCE;
    }

    public void doSomething() {

    }
}
