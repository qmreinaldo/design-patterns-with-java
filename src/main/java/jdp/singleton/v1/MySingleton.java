package jdp.singleton.v1;

public class MySingleton {
    /* Keep a reference of the instance in a static variable. */
    private static MySingleton instance;

    /* Make the constructor private to ensure only one copy of the instance. */
    private MySingleton() {

    }

    /* public method to return the instance.  */
    public static MySingleton getInstance() {
        if (instance == null)   // Lazy initialization
            return new MySingleton();
        return instance;
    }

    /* Any other method. */
    public void doSomething() {

    }
}
