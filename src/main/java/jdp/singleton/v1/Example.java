package jdp.singleton.v1;

public class Example {
    public static void main(String[] args) {
        var singleton1 = MySingleton.getInstance();
        singleton1.doSomething();

        var singleton2 = MySingleton.getInstance();
        singleton2.doSomething();
    }
}
