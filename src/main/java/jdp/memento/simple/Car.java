package jdp.memento.simple;

import java.util.Random;

public class Car {
    /*
    Inner class with a separate representation of the outer class's state
     */
    private class CarMemento implements Memento {
        /*
        Speed
         */
        private final int tempSpeed = Car.this.speed;

        /*
        Fuel consumption
         */
        private final int tempFuelConsumption = Car.this.currentFuelConsumption;

    }
    /*
    The speed of the car, initially 0
     */
    private int speed = 0;

    /*
    The fuel consumption, initially 0
     */
    private int currentFuelConsumption = 0;

    /**
     * Speed up quite a bit, recalculate fuel consumption
     */
    public void driveMuchFaster() {
        speed += 10;
        calculateFuelConsumption();
    }

    /**
     * Speed up slightly, recalculate fuel consumption
     */
    public void driveFaster() {
        speed++;
        calculateFuelConsumption();
    }

    /**
     * Slow down, recalculate fuel consumption
     */
    public void driveSlower() {
        if (speed > 0) {
            speed--;
            calculateFuelConsumption();
        }
    }

    /**
     * Calculate fuel consumption
     */
    private void calculateFuelConsumption() {
        double randomVal = (Math.random()) + 1.0;
        this.currentFuelConsumption = (int) (randomVal * speed);
    }

    @Override
    public String toString() {
        return "Car is driving with a speed of " + speed + "km/h"
                + "\nCurrent fuel consumption: " + currentFuelConsumption;
    }

    /**
     * Create a memento
     * @return a new memento with the current state of the car
     */
    public Memento createMemento() {
        return new CarMemento();
    }

    public void setMemento(Memento memento) {
        CarMemento myMemento = (CarMemento) memento;
        this.speed = myMemento.tempSpeed;
        this.currentFuelConsumption = myMemento.tempFuelConsumption;
    }

}
