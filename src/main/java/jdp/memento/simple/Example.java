package jdp.memento.simple;

import java.util.Stack;

public class Example {
    public static void main(String[] args) {
        final Stack<Memento> stack = new Stack<>();
        Memento memento;

        Car car = new Car();
        car.driveMuchFaster();
        System.out.println(car);
        memento = car.createMemento();
        stack.push(memento);

        car.driveMuchFaster();
        car.driveMuchFaster();
        System.out.println("\n" + car);
        memento = car.createMemento();
        stack.push(memento);

        car.driveSlower();
        System.out.println("\n" + car);
        memento = car.createMemento();
        stack.push(memento);

        car.driveFaster();
        System.out.println("\n" + car + "\n");

        System.out.println("Undo everything: ");
        while (!stack.isEmpty()) {
            car.setMemento(stack.pop());
            System.out.println("\n" + car);
        }
    }
}
