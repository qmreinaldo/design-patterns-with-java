package jdp.iterator.collections2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyList<E> implements Iterable<E> {
    private class Node<E> {
        private final E element;
        private Node<E> nextNode;

        Node(E element, Node<E> nextNode) {
            this.element = element;
            this.nextNode = nextNode;
        }
    }

    private int counter = 0;
    private Node<E> header = null;

    public int size() {
        return counter;
    }
    @SuppressWarnings("uncheked")
    public void add (E element) {
        header = new Node(element, header);
        counter++;
    }

    public boolean remover(E element) {
        Node<E> previous = null;
        Node<E> walk = header;

        while (walk != null) {
            if (equals(element, walk.element)) {
                if (previous == null)  // Remove the header dnd replace it
                    header = walk.nextNode;
                else
                    previous.nextNode = walk.nextNode;
                counter--;
                return true;
            }
            previous = walk;
            walk = walk.nextNode;
        }
        return false;
    }

    private boolean equals(E element1, E element2) {
        if (element1 == null)
            return element2 == null;
        else
            return element1.equals(element2);
    }

    public E get(int index) {
        if (index < 0 || index >= counter)
            throw new NoSuchElementException("Not valid index");
        Node<E> walk = header;
        for (int i = 0; i < index; i++) {
            walk = walk.nextNode;
        }
        return walk.element;
    }

    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private Node current = header;
            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public E next() {
                if (current == null)
                    throw new NoSuchElementException("No more data");
                E value = (E) current.element;
                current = current.nextNode;
                return value;
            }
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    public static void main(String[] args) {
        var myList = new MyList<String>();
        myList.add("String 1");
        myList.add("String 2");
        myList.add("String 3");
        myList.add("String 4");
        myList.add("String 5");

        Iterator<String> iterator = myList.iterator();
        while (iterator.hasNext())
            System.out.println(iterator.next());

        for (String str : myList)
            System.out.println(str);

        // Should throw an exception
        System.out.println(iterator.next());
    }
}
