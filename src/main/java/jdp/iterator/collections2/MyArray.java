package jdp.iterator.collections2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyArray<E> implements Iterable<E> {
    private int counter = 0;
    private static int DEFAULT_SIZE = 5;
    private Object[] elements = new Object[DEFAULT_SIZE];

    public void add(E e) {
        if (counter == elements.length) {
            Object[] tempArray = new Object[DEFAULT_SIZE+5];
            System.arraycopy(elements, 0, tempArray, 0, counter);
            elements = tempArray;
        }
        elements[counter] = e;
        counter++;
    }

    public void remove(int index) {
        if ((index <= counter) && (counter > 0) && (index >= 0)) {
            if (index != counter)
                // move over the right part of the array by one position
                System.arraycopy(elements, index + 1, elements, index, elements.length - 1 - index);
            elements[counter--] = null;
        }
    }

    public int size() {
        return counter;
    }

    public E get(int index) {
        @SuppressWarnings("uncheked")
        E element = (E) elements[index];
        return element;
    }

    /**
     * Return an iterator for MyArray
     * @return the iterator
     */
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            // At start, with an empty array, the position must be -1
            private int position = -1;

            /**
             * Check if there are more elements in the collection
             * @return true if there are more elements
             */
            @Override
            public boolean hasNext() {
                return (position < size()-1) && (elements[position+1] != null);
            }

            /**
             * Get the next element of the array
             * @return the type cast element
             */
            @Override
            public E next() {
                position++;

                if (position >= size() || elements[position] == null)
                    throw new NoSuchElementException("No more data");
                @SuppressWarnings("unchecked")
                E value = (E) elements[position];
                return value;
            }

            /**
             * Remove an element from the collection. Not implemented.
             */
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }


    public static void main(String[] args) {
        var myArray = new MyArray<String>();
        myArray.add("String 1");
        myArray.add("String 2");
        myArray.add("String 3");
        myArray.add("String 4");
        myArray.add("String 5");
        myArray.add("String 6");

        System.out.println(Arrays.toString(myArray.elements));

        var iterator = myArray.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        for (String str : myArray)
            System.out.println(str);

        // Should throw an exception
        System.out.println(iterator.next());
    }
}
