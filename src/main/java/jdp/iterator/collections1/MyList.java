package jdp.iterator.collections1;

import java.util.NoSuchElementException;

public class MyList<E> {
    private class Node<E> {
        private final E element;
        private Node<E> nextNode;

        Node(E element, Node<E> nextNode) {
            this.element = element;
            this.nextNode = nextNode;
        }
    }

    private int counter = 0;
    private Node<E> header = null;

    public int size() {
        return counter;
    }
    @SuppressWarnings("uncheked")
    public void add (E element) {
        header = new Node(element, header);
        counter++;
    }

    public boolean remover(E element) {
        Node<E> previous = null;
        Node<E> walk = header;

        while (walk != null) {
            if (equals(element, walk.element)) {
                if (previous == null)  // Remove the header dnd replace it
                    header = walk.nextNode;
                else
                    previous.nextNode = walk.nextNode;
                counter--;
                return true;
            }
            previous = walk;
            walk = walk.nextNode;
        }
        return false;
    }

    private boolean equals(E element1, E element2) {
        if (element1 == null)
            return element2 == null;
        else
            return element1.equals(element2);
    }

    public E get(int index) {
        if (index < 0 || index >= counter)
            throw new NoSuchElementException("Not valid index");
        Node<E> walk = header;
        for (int i = 0; i < index; i++) {
            walk = walk.nextNode;
        }
        return walk.element;
    }
}
