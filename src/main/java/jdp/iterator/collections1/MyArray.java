package jdp.iterator.collections1;

public class MyArray<E> {
    private int counter = 0;
    private static int DEFAULT_SIZE = 5;
    private Object[] elements = new Object[DEFAULT_SIZE];

    public void add(E e) {
        if (counter == elements.length) {
            Object[] tempArray = new Object[2 * DEFAULT_SIZE];
            System.arraycopy(elements, 0, tempArray, 0, counter);
            elements = tempArray;
        }
        elements[counter] = e;
        counter++;
    }

    public void remove(int index) {
        if ((index <= counter) && (counter > 0) && (index >= 0)) {
            if (index != counter)
                // move over the right part of the array by one position
                System.arraycopy(elements, index + 1, elements, index, elements.length - 1 - index);
            elements[counter--] = null;
        }
    }

    public int size() {
        return counter;
    }

    public E get(int index) {
        @SuppressWarnings("uncheked")
        E element = (E) elements[index];
        return element;
    }
}
