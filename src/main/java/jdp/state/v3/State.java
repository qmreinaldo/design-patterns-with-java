package jdp.state.v3;

public abstract class State {
    /*
    The state object for OPEN
     */
    protected static final State OPEN = new Open();
    /*
    The state object for CLOSED
     */
    protected static final State CLOSED = new Closed();
    /*
    The state object for LOCKED
     */
    protected static final State LOCKED = new Locked();

    abstract State open();
    abstract State close();
    abstract State lock();
    abstract State unlock();

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
