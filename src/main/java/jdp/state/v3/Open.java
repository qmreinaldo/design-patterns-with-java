package jdp.state.v3;

public class Open extends State {

    /*
     * Open is not possible
     */
    @Override
    public State open() {
        System.out.println("The gate is already open!");
        return this;
    }


    /*
     * Close will work
     */
    @Override
    public State close() {
        System.out.println("The gate will be closed.");
        return super.CLOSED;
    }

    /*
    Lock is not possible
     */
    @Override
    public State lock() {
        System.out.println("Close the gate before lock.");
        return this;
    }

    /*
    Unlock also is not possible.
     */
    @Override
    public State unlock() {
        System.out.println("The gate is not locked, so it cannot be unlocked.");
        return this;
    }
}
