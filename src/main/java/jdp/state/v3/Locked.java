package jdp.state.v3;

public class Locked extends State {

    /*
    Open is not possible
     */
    @Override
    public State open() {
        System.out.println("The gate is locked and cannot be opened.");
        return this;
    }

    /*
    Close is not possible
     */
    @Override
    public State close() {
        System.out.println("The gate is locked and cannot be locked");
        return this;
    }

    /*
    Lock is not possible
     */
    @Override
    public State lock() {
        System.out.println("The gate is already locked!");
        return this;
    }

    /**
     * Unlock will work
     */
    @Override
    public State unlock() {
        System.out.println("The gate will be unlocked");
        return super.CLOSED;
    }
}
