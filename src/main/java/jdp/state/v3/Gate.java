package jdp.state.v3;

public class Gate {
    private State state = new Open();

    public void open() {
        this.state = state.open();
    }

    public void close() {
        this.state = state.close();
    }

    public void lock() {
        this.state = state.lock();
    }

    public void unlock() {
        this.state = state.unlock();
    }

    @Override
    public String toString() {
        return "The gate is " + state + ".";
    }
}
