package jdp.state.v3;

public class Closed extends State {

    /*
    Open will work
     */
    @Override
    public State open() {
        System.out.println("The gate will be opened");
        return super.OPEN;
    }

    /*
    Close is not possible
     */

    @Override
    public State close() {
        System.out.println("The gate is already closed!");
        return this;
    }

    /*
    Lock is possible
     */
    @Override
    public State lock() {
        System.out.println("The gate will be locked.");
        return super.LOCKED;
    }

    /*
    Unlock is not possible
     */
    @Override
    public State unlock() {
        System.out.println("The gate is not locked and cannot be unlocked.");
        return this;
    }
}
