package jdp.state.gatemanager;

public class GateManager {
    /*
    Inner class for the valid states.
     */
    public static class State {
        /*
        State open
         */
        public static final State OPEN = new State("open");

        /*
        State closed
         */
        public static final State CLOSED = new State("closed");

        private final String description;

        /*
        Constructor is not reachable from outside.
         */
        private State(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return "The gate is " + this.description;
        }
    }
    private State state = State.OPEN;

    void printState() {
        System.out.println(state);
    }

    void setState(State state) {
        if (state != null)
            this.state = state;
    }

    public static void main(String[] args) {
        GateManager manager = new GateManager();

        manager.setState(State.OPEN);
        manager.printState();

        manager.setState(State.CLOSED);
        manager.printState();
    }
}
