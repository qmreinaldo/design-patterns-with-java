package jdp.state.gatemanager;

public class GateManagerChangeState {
    enum State {
        OPEN("open"), CLOSED("closed");

        private final String description;

        private State(String description) {
            this.description = description;
        }
    }

    private State state = State.OPEN;

    /*
    Set OPEN if not set already
     */
    private void open() {
        if (state == State.CLOSED) {
            System.out.println("The gate is opened");
            this.state = State.OPEN;
        }
        else
            System.out.println("Gate is already open!");
    }

    private void close() {
        if (state == State.OPEN) {
            System.out.println("The gate is closed");
            this.state = State.CLOSED;
        }
        else
            System.out.println("Gate is already closed!");
    }

    public void printState() {
        System.out.println("State: " + state);
    }

    public static void main(String[] args) {
        var demoState = new GateManagerChangeState();

        demoState.printState();
        demoState.close();
        demoState.printState();
        demoState.open();
        demoState.printState();
        demoState.open();
    }
}
