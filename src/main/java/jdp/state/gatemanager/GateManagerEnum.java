package jdp.state.gatemanager;

public class GateManagerEnum {
    enum State {
        /*
        Constructor is called implicitly
         */
        OPEN("open"), CLOSED("closed");

        private final String description;

        private State (String description) {
            this.description = description;
        }
    }
    private State state = State.OPEN;

    void printState() {
        switch (state) {
            case OPEN -> System.out.println("Gate is open");
            case CLOSED -> System.out.println("Gate is closed");
        }
    }

    void setState(State state) {
        if (state != null)
            this.state = state;
    }

    public static void main(String[] args) {
        GateManagerEnum manager = new GateManagerEnum();

        manager.setState(State.OPEN);
        manager.printState();

        manager.setState(State.CLOSED);
        manager.printState();
    }
}
