package jdp.state.v1;

public class Client {
    private static final Gate GATE = new Gate();

    public static void main(String[] args) {
        System.out.println(GATE + "\n\t");
        GATE.open();
        System.out.println(GATE + "\n\t");
        GATE.close();
        System.out.println(GATE + "\n\t");
        GATE.open();
        System.out.println(GATE + "\n\t");
        GATE.unlock();
        System.out.println(GATE + "\n\t");
        GATE.lock();
        System.out.println(GATE + "\n\t");
        GATE.close();
        System.out.println(GATE + "\n\t");
        GATE.lock();
        System.out.println(GATE + "\n\t");
        GATE.open();
        System.out.println(GATE + "\n\t");
        GATE.unlock();
        System.out.println(GATE + "\n\t");
        GATE.open();
        System.out.println(GATE + "\n\t");
    }
}
