package jdp.state.v1;

public class Open extends State {
    Open(Gate gate) {
        super(gate);
    }

    /*
     * Open is not possible
     */
    @Override
    public void open() {
        System.out.println("The gate is already open!");
    }


    /*
     * Close will work
     */
    @Override
    public void close() {
        System.out.println("The gate will be closed.");
        gate.setState(new Closed(gate));
    }

    /*
    Lock is not possible
     */
    @Override
    void lock() {
        System.out.println("Close the gate before lock.");
    }

    /*
    Unlock also is not possible.
     */
    @Override
    void unlock() {
        System.out.println("The gate is not locked, so it cannot be unlocked.");
    }

}
