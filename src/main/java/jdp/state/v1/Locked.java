package jdp.state.v1;

public class Locked extends State {

    Locked(Gate gate) {
        super(gate);
    }

    /*
    Open is not possible
     */
    @Override
    public void open() {
        System.out.println("The gate is locked and cannot be opened.");
    }

    /*
    Close is not possible
     */
    @Override
    public void close() {
        System.out.println("The gate is locked and cannot be locked");
    }

    /*
    Lock is not possible
     */
    @Override
    public void lock() {
        System.out.println("The gate is already locked!");
    }

    /**
     * Unlock will work
     */
    @Override
    public void unlock() {
        System.out.println("The gate will be unlocked");
        gate.setState(new Closed(gate));
    }
}
