package jdp.state.v1;

public abstract class State {
    /*
    The gate will be stored in the superclass.
     */
    public final Gate gate;

    /*
    The state know their gate from the beginning.
     */
    State(Gate gate) {
        this.gate = gate;
    }

    abstract void open();
    abstract void close();
    abstract void lock();
    abstract void unlock();

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
