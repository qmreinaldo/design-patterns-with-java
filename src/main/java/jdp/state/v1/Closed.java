package jdp.state.v1;

public class Closed extends State {
    Closed (Gate gate) {
        super(gate);
    }

    /*
    Open will work
     */
    @Override
    public void open() {
        System.out.println("The gate will be opened");
        gate.setState(new Open(gate));
    }

    /*
    Close is not possible
     */

    @Override
    public void close() {
        System.out.println("The gate is already closed!");
    }

    /*
    Lock is possible
     */
    @Override
    public void lock() {
        System.out.println("The gate will be locked.");
        gate.setState(new Locked(gate));
    }

    /*
    Unlock is not possible
     */
    @Override
    public void unlock() {
        System.out.println("The gate is not locked and cannot be unlocked.");
    }
}
