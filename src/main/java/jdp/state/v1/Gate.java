package jdp.state.v1;

public class Gate {
    private State state = new Open(this);

    /**
     * Set a new state for the gate
     *
     * @param state - the new state
     */
    public void setState(State state) {
        this.state = state;
    }

    /*
    Change the state to Open if possible
     */
    public void open() {
        state.open();
    }

    /*
    Change state to Closed if possible
     */
    public void close() {
        state.close();
    }

    /*
    Change state to Locked if possible
     */
    public void lock() {
        state.lock();
    }

    /*
    Change state to Closed if possible
     */
    public void unlock() {
        state.unlock();
    }

    @Override
    public String toString() {
        return "The gate is " + state + ".";
    }
}
