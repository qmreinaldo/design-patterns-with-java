package jdp.template.problem;

import javax.swing.*;

public class Lowercase {
    public final void run() {
        final var input = textEnter();
        final var converted = convert(input);
        print(converted);
    }

    private void print(String text) {
        System.out.println(text);
    }

    private String convert(String input) {
        return input.toLowerCase();
    }

    private String textEnter() {
        final var message = "Please enter the text: ";
        return JOptionPane.showInputDialog(message);
    }

    public static void main (String[] args) {
        new Lowercase().run();
    }
}
