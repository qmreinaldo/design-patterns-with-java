package jdp.template.v1;

public class UppercaseConverter extends Input {

    @Override
    protected String convert(String input) {
        return input.toUpperCase();
    }
}
