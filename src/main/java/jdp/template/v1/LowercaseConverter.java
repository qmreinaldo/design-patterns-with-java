package jdp.template.v1;

public class LowercaseConverter extends Input {

    @Override
    protected String convert(String input) {
        return input.toLowerCase();
    }
}
