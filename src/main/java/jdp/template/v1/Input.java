package jdp.template.v1;

import javax.swing.*;

public abstract class Input {

    /* Use final to make the algorithm bind all subclasses */
    public final void run() {
        var input = textEnter();
        var converted = convert(input);
        print(converted);
    }
    private final String textEnter() {
        return JOptionPane.showInputDialog("Please enter the text: ");
    }

    /* This method should be overridden in the subclasses. */
    protected abstract String convert(String input);

    private final void print (String text) {
        System.out.println(text);
    }
}
