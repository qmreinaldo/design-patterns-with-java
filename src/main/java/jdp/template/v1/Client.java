package jdp.template.v1;

public class Client {
    public static void main(String[] args) {
        Input input =  new LowercaseConverter();
        input.run(); // The call is always made from the superclass.

        Input anotherInput = new UppercaseConverter();
        anotherInput.run();
    }
}
