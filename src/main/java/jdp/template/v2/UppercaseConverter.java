package jdp.template.v2;

public class UppercaseConverter extends Input {

    @Override
    protected String convert(String input) {
        return input.toUpperCase();
    }

    @Override
    protected boolean save() {
        return true;
    }
}