package jdp.template.v2;

public class Client {
    public static void main(String[] args) {
        Input input =  new LowercaseConverter();
        input.run();

        Input anotherInput = new UppercaseConverter();
        anotherInput.run();
    }
}