package jdp.template.v2;

import javax.swing.*;

public abstract class Input {
    public final void run() {
        var input = textEnter();
        var converted = convert(input);
        print(converted);
        if (save())
            saveToDisk();
    }
    private final String textEnter() {
        return JOptionPane.showInputDialog("Please enter the text: ");
    }

    /* This method should be overridden in the subclasses. */
    protected abstract String convert(String input);

    private final void print (String text) {
        System.out.println(text);
    }

    /* hook method */
    protected boolean save() {
        return false; // by default
    }

    private void saveToDisk() {
        System.out.println("Input saved");
    }
}
