package jdp.template.v2;

import javax.swing.*;

public class LowercaseConverter extends Input {

    @Override
    protected String convert(String input) {
        return input.toLowerCase();
    }

    @Override
    protected boolean save() {
        var question = "Should the text be saved?";
        var answer = JOptionPane.showConfirmDialog(null, question);
        return answer == JOptionPane.YES_OPTION;
    }
}
