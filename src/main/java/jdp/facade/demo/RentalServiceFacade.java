package jdp.facade.demo;

import jdp.facade.trip.carrental.RentalCar;

public class RentalServiceFacade {
    /**
     * Facade of the car rental service.
     * Execute the basic tasks
     */
    public static void rentCar() {
        var rentalCar = new RentalCar();
        rentalCar.insure();
        rentalCar.refuel();
    }
}
