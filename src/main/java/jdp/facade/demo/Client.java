package jdp.facade.demo;

import jdp.facade.trip.hotel.AllIn;

public class Client {
    public static void main(String[] args) {
        System.out.println("A client:");
        new TravelAgencyFacade().bookTrip(false);

        System.out.println("\nAnother client:");
        new TravelAgencyFacade().bookTrip(true);
    }
}
