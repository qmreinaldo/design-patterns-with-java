package jdp.facade.demo;

import jdp.facade.trip.Flight;
import jdp.facade.trip.RailAndFly;
import jdp.facade.trip.Transfer;
import jdp.facade.trip.hotel.AllIn;
import jdp.facade.trip.hotel.Hotel;

public class TravelAgencyFacade {
    /**
     * It enables the client to book a trip
     * @param withRentalCar - if the client needs a rental car
     */
    public void bookTrip(boolean withRentalCar) {
        var railAndFly = new RailAndFly();
        var flight = new Flight();
        var transfer = new Transfer();
        var hotel = new Hotel();
        var allIn = new AllIn();
        if (withRentalCar)
            RentalServiceFacade.rentCar();
    }
}
