package jdp.facade.trip;

public class RailAndFly {
    public RailAndFly() {
        System.out.println("You take the train to the airport");
    }

    public void board() {
        System.out.println("You board the train");
    }
}
