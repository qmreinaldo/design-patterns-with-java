package jdp.facade.trip.carrental;

public class RentalCar {
    public RentalCar() {
        System.out.println("You get a rental car");
    }

    public void insure() {
        new CarInsurance();
        System.out.println("The car is insured");
    }

    public void refuel() {
        System.out.println("The car is refueled");
    }
}
