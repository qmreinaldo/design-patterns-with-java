package jdp.facade.trip.hotel;

public class Hotel {
    public Hotel() {
        System.out.println("The hotel room is reserved.");
    }

    public void rentSafe() {
        System.out.println("You rented a safe");
    }
}
