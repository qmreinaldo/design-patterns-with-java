package jdp.facade.trip.hotel;

public class Breakfast extends Board {
    public Breakfast() {
        System.out.println("You only get breakfast");
    }
}
