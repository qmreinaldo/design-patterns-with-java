package jdp.facade.trip.hotel;

public class HalfBoard extends Board {
    public HalfBoard() {
        System.out.println("You get half board");
    }

    public void orderBeer() {
        System.out.println("You ordered a beer");
    }
}
