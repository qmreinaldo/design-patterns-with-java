package jdp.facade.trip;

public class Transfer {
    public Transfer() {
        System.out.println("You will be transferred from airport to hotel");
    }

    public void loadBabbage() {
        System.out.println("Your babbage is loaded into the bus");
    }

}
