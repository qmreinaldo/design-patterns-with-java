package jdp.facade.trip;

public class Flight {
    public Flight() {
        System.out.println("Flight is booked");
    }

    public void checkBaggage() {
        System.out.println("Baggage is checked");
    }

    public void identify() {
        System.out.println("You identify yourself");
    }
}
