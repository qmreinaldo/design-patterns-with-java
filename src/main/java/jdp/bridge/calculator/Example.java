package jdp.bridge.calculator;

public class Example {
    public static void main(String[] args) {
        // Using default implementation
        Implementor implementor = new Implementor();
        Calculator calculator = new Calculator(implementor);
        System.out.println("19 * 23 = " + calculator.multiply(19, 23));

        // Switch to a new implementor
        implementor = new SuperFastMultiplication();
        calculator = new Calculator(implementor);
        System.out.println("19 * 23 = " + calculator.multiply(19, 23));

        // Another switch of implementor
        calculator.setImplementor(new FastFourierTransformation());
        System.out.println("19 * 23 = " + calculator.multiply(19, 23));

        CalculatorDeluxe calculatorDeluxe = new CalculatorDeluxe(implementor);
        System.out.println("19^2 = " + calculatorDeluxe.square(19.0));

        CalculatorNewton calculatorNewton = new CalculatorNewton(new FastFourierTransformation());
        System.out.println("sqrt(25) = " + calculatorNewton.root(25));
    }
}
