package jdp.bridge.calculator;

import java.security.spec.RSAOtherPrimeInfo;

public class SuperFastMultiplication extends Implementor {
    @Override
    double multiply(double factor_1, double factor_2) {
        // Only simulates a fancy algorithm for multiplication
        System.out.println("Super fast multiplication");
        return factor_1 * factor_2;
    }
}
