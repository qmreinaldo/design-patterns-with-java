package jdp.bridge.calculator;

public class CalculatorDeluxe extends Calculator {
    /**
     * Constructor. It also need an implementor
     * @param implementor
     */
    public CalculatorDeluxe(Implementor implementor) {
        super(implementor);
    }

    /**
     * Square a number
     * @param number
     * @return the square
     */
    public double square(double number) {
        return super.multiply(number, number);
    }
}
