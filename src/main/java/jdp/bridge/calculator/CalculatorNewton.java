package jdp.bridge.calculator;

public class CalculatorNewton extends CalculatorDeluxe {
    /**
     * Constructor. It also needs an implementor
     * @param implementor
     */
    public CalculatorNewton(Implementor implementor) {
        super(implementor);
    }

    /**
     * Calculate the square root
     * @param number
     * @return
     */
    public double root(double number) {
        // Only simulates the Newton method
        return Math.sqrt(number);
    }
}
