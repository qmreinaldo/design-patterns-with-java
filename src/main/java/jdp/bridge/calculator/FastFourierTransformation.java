package jdp.bridge.calculator;

public class FastFourierTransformation extends Implementor {
    @Override
    double multiply(double factor_1, double factor_2) {
        // Only simulates the FFT algorithm
        System.out.println("FFT-Multiplication");
        return factor_1 * factor_2;
    }
}
