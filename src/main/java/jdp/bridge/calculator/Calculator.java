package jdp.bridge.calculator;

public class Calculator {
    /*
    A reference to the object implementing the actual methods
     */
    private Implementor implementor;

    /**
     * Constructor. It gets the implementor
     * @param implementor
     */
    public Calculator(Implementor implementor) {
        this.implementor = implementor;
    }

    /**
     * Set a new implementor
     * @param implementor
     */
    public void setImplementor(Implementor implementor) {
        this.implementor = implementor;
    }

    /**
     * Add the two given numbers
     * @param summand_1
     * @param summand_2
     * @return sum
     */
    public double add(double summand_1, double summand_2) {
        return implementor.add(summand_1, summand_2);
    }

    /**
     * Subtract the two given numbers
     * @param minuend
     * @param subtrahend
     * @return the difference
     */
    public double subtract(double minuend, double subtrahend) {
        return implementor.subtract(minuend, subtrahend);
    }

    /**
     * Multiply the two given numbers
     * @param factor_1
     * @param factor_2
     * @return the product
     */
    public double multiply(double factor_1, double factor_2) {
        return implementor.multiply(factor_1, factor_2);
    }

    /**
     * Divide the two given numbers
     * @param dividend
     * @param divisor
     * @return the quotient
     */
    public double divide(double dividend, double divisor) {
        return implementor.divide(dividend, divisor);
    }


}
