package jdp.adapter.classbased;

import java.util.Collections;
import java.util.List;

public class SorterExternalProduct {
    /**
     * Sort, using a list as input parameter
     * @param numberList the list to be sorted
     * @return the sorted list
     */
    List<Integer> sort(List<Integer> numberList) {
        Collections.sort(numberList);
        return numberList;
    }
}
