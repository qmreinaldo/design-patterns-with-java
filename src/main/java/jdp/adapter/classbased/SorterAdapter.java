package jdp.adapter.classbased;

import java.util.ArrayList;
import java.util.List;

public class SorterAdapter extends SorterExternalProduct implements Sorter {

    /**
     * The adapter converts the parameter in both directions
     * @param numbers- an array to be sorted
     * @return the sorted array
     */
    @Override
    public int[] sort(int[] numbers) {
        int[] ans = new int[numbers.length];
        List<Integer> numberList = new ArrayList<>();
        for (int number : numbers)
            numberList.add(number);
        var sortedList = sort(numberList);
        for (int i = 0; i < sortedList.size(); i++)
            ans[i] = sortedList.get(i);
        return ans;
    }
}
