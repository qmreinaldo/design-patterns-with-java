package jdp.adapter.classbased;

import java.util.Arrays;

public class Client {
    public static void main(String[] args) {
        final int[] numbers = new int[] {13, 7, 5, 3, 2};
        SorterAdapter sorter = new SorterAdapter();
        int[] sorted = sorter.sort(numbers);
        System.out.println(Arrays.toString(sorted));
    }
}
