package jdp.adapter.classbased;

public interface Sorter {
    /**
     * The original interface known to and used by the client
     * @param numbers- an array to be sorted
     * @return the sorted array
     */
    int[] sort(int... numbers);
}
