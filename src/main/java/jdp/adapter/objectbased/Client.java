package jdp.adapter.objectbased;

import java.util.Arrays;

public class Client {
    public static void main(String[] args) {
        final int[] numbers = new int[] {13, 11, 7, 5, 3, 2};
        Sorter sorter = new SorterAdapter();
        int[] sorted = sorter.sort(numbers);
        System.out.println(Arrays.toString(sorted));
    }
}
