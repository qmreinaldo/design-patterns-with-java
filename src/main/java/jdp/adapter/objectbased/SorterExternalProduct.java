package jdp.adapter.objectbased;

import java.util.Collections;
import java.util.List;

public final class SorterExternalProduct {
    List<Integer> sort(List<Integer> numberList) {
        Collections.sort(numberList);
        return numberList;
    }
}
