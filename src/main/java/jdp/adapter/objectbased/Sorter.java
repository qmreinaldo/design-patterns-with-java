package jdp.adapter.objectbased;

public interface Sorter {
    int[] sort(int... numbers);
}
