package jdp.adapter.objectbased;

import java.util.ArrayList;
import java.util.List;

public class SorterAdapter implements Sorter {
    /*
    An object for the sorter to be called
     */
    private final SorterExternalProduct externalProduct = new SorterExternalProduct();

    /**
     * The adapter
     * @param numbers the array to be sorted
     * @return the sorted array
     */
    @Override
    public int[] sort(int[] numbers) {
        int[] ans = new int[numbers.length];
        List<Integer> numeberList = new ArrayList<>();
        for (int num : numbers)
            numeberList.add(num);
        List<Integer> sortedList = externalProduct.sort(numeberList);
        for (int i = 0; i < sortedList.size(); i++)
            ans[i] = sortedList.get(i);
        return ans;
    }
}
