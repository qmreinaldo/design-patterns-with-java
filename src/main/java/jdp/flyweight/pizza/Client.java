package jdp.flyweight.pizza;

import java.util.HashMap;
import java.util.Map;

public class Client {
    /*
    Orders
     */
    private static final Map<Integer, MenuEntry[]> ORDERS = new HashMap<>();

    private static final MenuFactory MENU_FACTORY = new MenuFactory();

    /**
     * Take a new order
     * @param table - the number of the table
     * @param menu - the menu
     */
    public static void takeOrder(int table, String... menu) {
        MenuEntry[] order = MENU_FACTORY.getMenu(menu);
        ORDERS.put(table, order);
    }

    public static void main(String[] args) {
        takeOrder(1, "Pizza Hawaii");
        takeOrder(2, "Pizza Salami");
        takeOrder(3, "Pizza Hawaii");
        ORDERS.keySet().forEach(table -> {
            MenuEntry[] menus = ORDERS.get(table);
            for (MenuEntry menu : menus)
                menu.serve(table);
        });
    }


}
