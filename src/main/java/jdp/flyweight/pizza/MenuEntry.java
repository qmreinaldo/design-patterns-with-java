package jdp.flyweight.pizza;

public interface MenuEntry {
    /**
     * Deliver all orders to the given table
     * @param table - the number of the table
     */
    void serve(int table);
}
