package jdp.flyweight.pizza;

import java.util.HashMap;
import java.util.Map;

public class MenuFactory {
    /*
    Hold all the menus
     */
    private final Map<String, MenuEntry> menus = new HashMap<>();

    /**
     * Checks whether a menu has been ordered once. Adds it if not
     * @param name - the new menu
     * @return the list of menus
     */
    public MenuEntry[] getMenu(String... name) {
        int numberOfOrders = name.length;
        MenuEntry[] result = new MenuEntry[numberOfOrders];
        for (int i = 0; i < numberOfOrders; i++) {
            MenuEntry menu = menus.get(name[i]);
            if (menu == null) {
                menu = new Pizza(name[i]);
                menus.put(name[i], menu);
            }
            result[i] = menu;
        }
        return result;
    }
}
