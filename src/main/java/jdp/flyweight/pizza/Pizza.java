package jdp.flyweight.pizza;

public class Pizza implements MenuEntry {
    /*
    The name of the pizza
     */
    public final String name;

    public Pizza(String name) {
        this.name = name;
    }
    @Override
    public void serve(int table) {
        System.out.println(name + " is served to the table " + table);
    }
}
