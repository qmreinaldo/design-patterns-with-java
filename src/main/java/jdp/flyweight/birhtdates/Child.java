package jdp.flyweight.birhtdates;

import java.time.LocalDate;

public class Child {
    private final long id;
    private final String name;
    private final String surname;
    private final LocalDate dateOfBirth;

    public Child(String name, String surname, LocalDate dateOfBirth) {
        this.name = name;
        this.surname = name;
        this.dateOfBirth = dateOfBirth;
        id = (long) (Long.MAX_VALUE * Math.random()
                * (name.hashCode() + surname.hashCode() + dateOfBirth.hashCode()));
    }
}
