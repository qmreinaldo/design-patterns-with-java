package jdp.flyweight.birhtdates;

import jdp.flyweight.birhtdates.Child;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashSet;
import java.time.LocalDate;

public class Factory {
    /*
    A list of all occurring names
     */
    private final HashSet<String> nameSet = new HashSet<>();

    /*
    A list of all occurring surnames
     */
    private final HashSet<String> surnameSet = new HashSet<>();

    /*
    A list of all occurring dates of birth
     */
    private final HashSet<LocalDate> dateOfBirthSet = new HashSet<>();

    /**
     * The factory does not store duplicate elements, but adds some if necessary
     * @param name first name
     * @param surname family name
     * @param dateOfBirth
     * @return a new Child object
     */
    Child getChild(String name, String surname, LocalDate dateOfBirth) {
        nameSet.add(name);
        surnameSet.add(surname);
        dateOfBirthSet.add(dateOfBirth);

        return new Child(name, surname, dateOfBirth);
    }

    void evaluate() {
        System.out.println("Number of stored first names: " + nameSet.size());
        nameSet.forEach(name -> {
            System.out.println("\t " + name);
        });

        System.out.println("\nNumber of stored family names: " + surnameSet.size());
        surnameSet.forEach(surname -> {
            System.out.println("\t" + surname);
        });

        System.out.println("\nNumber of stored birthdays: " + dateOfBirthSet.size());
        dateOfBirthSet.forEach(dateOfBirthSet -> {
            System.out.println("\t" + dateOfBirthSet.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));
        });
    }
}
