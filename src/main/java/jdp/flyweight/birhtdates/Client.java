package jdp.flyweight.birhtdates;

import java.time.LocalDate;

public class Client {
    public static void main(String[] args) {
        Factory factory = new Factory();
        Child child1 = factory.getChild("Lionel", "Messi", LocalDate.now());
        Child child2 = factory.getChild("Antonella", "Messi", LocalDate.now());
        Child child3 = factory.getChild("Thiago", "Messi", LocalDate.now());
        Child child4 = factory.getChild("Mateo", "Messi", LocalDate.now());
        Child child5 = factory.getChild("Ciro", "Messi", LocalDate.now());

        factory.evaluate();
    }
}
