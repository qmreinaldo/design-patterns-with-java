package jdp.abstractfactory.game;

public class EnterDoor implements Command {

    /**
     * This command start with "enter door"
     * @param cmdLine- the input
     * @return result of the check
     */
    @Override
    public boolean matches(CmdLine cmdLine) {
        return cmdLine.startsWith("enter", "door");
    }

    /**
     * Walk through the door in the given direction
     * @param cmdLine- the command
     * @param player- the player
     */
    @Override
    public void handle(CmdLine cmdLine, Player player) {
        if (cmdLine.hasDirection(2)) {
            Direction direction = cmdLine.getAsDirection(2);
            Room currentRoom = player.getCurrentRoom();
            Component component = currentRoom.getComponentAt(direction);
            if (component instanceof Door door)
                door.enter(player);
            else
                System.out.println("Ouch!");
        } else
            System.out.println("Which door would you like to walk through?");
    }
}
