package jdp.abstractfactory.game;

public class OpenDoor implements Command {

    /**
     * The command starts with "open door"
     * @param cmdLine- the input
     * @return of the check
     */
    @Override
    public boolean matches(CmdLine cmdLine) {
        return cmdLine.startsWith("open", "door");
    }

    /**
     * If there is a door in the given direction, it can be opened
     * @param cmdLine- the command
     * @param player- the player
     */
    @Override
    public void handle(CmdLine cmdLine, Player player) {
        if (cmdLine.hasDirection(2)) {
            Direction direction = cmdLine.getAsDirection(2);
            Room currentRoom = player.getCurrentRoom();
            Component component = currentRoom.getComponentAt(direction);

            if (component instanceof  Door door)
                door.open();
            else
                System.out.println("You can only open a door!");
        }
        else
            System.out.println("Which door would you like to open?'");
    }
}
