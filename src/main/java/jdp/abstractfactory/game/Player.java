package jdp.abstractfactory.game;

public class Player {

    /*
    The actual room where the player is
     */
    private Room currentRoom;

    public void setCurrentRoom(Room room) {
        currentRoom = room;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }
}
