package jdp.abstractfactory.game;

/**
 * A command line is split into single words. Every command knows
 * how it hast to look like and can check itself against the user's wish
 */
public class CmdLine {
    /*
    The individual parts of a command
     */
    private final String[] tokens;

    /**
     * Split an input into individual words
     *
     * @param nextLine
     */
    public CmdLine(String nextLine) {
        this.tokens = nextLine.split(" ");
    }

    /**
     * Check if the input stats with a specific sequence of words.
     *
     * @param tokens - the word sequence to check against the input
     * @return true, if the input starts with the same given words
     */
    public boolean startsWith(String... tokens) {
        if (this.tokens.length < tokens.length)
            return false;
        for (int i = 0; i < tokens.length; i++)
            if (!this.tokens[i].equalsIgnoreCase(tokens[i]))
                return false;
        return true;
    }

    /**
     * Check if there is a direction at a specific position in a command.
     *
     * @param index position to be checked
     * @return true, if there is a direction at this position
     */
    public boolean hasDirection(int index) {
        Direction dir;
        try {
            dir = Direction.getDirection(tokens[index]);
        } catch (Exception e) {
            return false;
        }
        if (dir == Direction.UNKNOWN)
            return false;
        return true;
    }

    /**
     * Convert a word into a Direction
     *
     * @param index- position of the word
     * @return the direction
     */
    public Direction getAsDirection(int index) {
        return Direction.getDirection(tokens[index]);
    }
}