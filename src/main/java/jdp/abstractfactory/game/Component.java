package jdp.abstractfactory.game;

public abstract class Component {
    /**
     * Enter a room
     * @param player- who enters the room
     */
    protected abstract void enter(Player player);

    /**
     * Get the description of a component
     * @return the description
     */
    protected abstract String describe();
}
