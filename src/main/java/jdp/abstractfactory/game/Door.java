package jdp.abstractfactory.game;

public class Door extends Component {
    /*
    The state of the door
     */
    private boolean isOpen = false;

    /*
     The first room the door connects
     */
    private final Room room1;

    /*
     The second room the door connects
     */
    private final Room room2;

    /**
     * A door always connect two rooms
     * @param room1 from
     * @param room2 to
     */
    Door(Room room1, Room room2) {
        this.room1 = room1;
        this.room2 = room2;
    }

    /**
     * Ask the door to which room it connects from a specific room
     * @param room - the given room
     * @return the other room the door connects to
     */
    Room getNeighbor(Room room) {
        return room == room1 ? room2 : room1;
    }

    public void open() {
        System.out.println("The door is open");
        isOpen = true;
    }

    public void close() {
        System.out.println("The door will be closed");
        isOpen = false;
    }

    /**
     * Walk through the door if possible
     * @param player- who enters the room
     */
    @Override
    protected void enter(Player player) {
        if (isOpen) {
            Room currentRoom = player.getCurrentRoom();
            Room newRoom = currentRoom == room1 ? room2 : room1;
            newRoom.enter(player);
        }
        else
            System.out.println("The door is closed.");
    }

    @Override
    protected String describe() {
        return isOpen ? " an open door" : " a closed door";
    }

    @Override
    public String toString() {
        return describe();
    }
}
