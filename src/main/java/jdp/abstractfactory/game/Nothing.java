package jdp.abstractfactory.game;

public class Nothing implements Command {

    /**
     * Whatever has been entered
     * @param cmdLine- the input
     * @return always true
     */
    @Override
    public boolean matches(CmdLine cmdLine) {
        return true;
    }

    /**
     * Unknown command
     * @param cmdLine- the command
     * @param player- the player
     */
    @Override
    public void handle(CmdLine cmdLine, Player player) {
        System.out.println("Wrong entry. Try it again.");
    }
}
