package jdp.abstractfactory.game;

public class Wall extends Component {

    /**
     * You cannot enter a wall
     * @param player
     */
    @Override
    protected void enter(Player player) {
        System.out.println("You step into a wall");
    }


    @Override
    protected String describe() {
        return " a wall";
    }

    @Override
    public String toString() {
        return describe();
    }
}
