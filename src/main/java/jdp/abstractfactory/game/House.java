package jdp.abstractfactory.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class House {
    /*
    Where the game starts
     */
    private final Room entry;

    /*
    The architect building the house
     */
    private final Architect architect;

    /**
     * Player commands
     */
    private final List<Command> commands = new ArrayList<>();

    /**
     * Constructor calls the architect to get the house built and the
     * entrance room delivered back. Also specify commands.
     */
    private House() {
        architect = new Architect(new ComponentFactory());
        entry = architect.buildHouse();

        commands.add(new OpenDoor());
        commands.add(new EnterDoor());
        commands.add(new LookAround());
        commands.add(new Exit());
        commands.add(new Nothing());
    }

    /**
     * Enter the house, look around and take commands until the game is quit.
     */
    private void enter() {
        System.out.println(entry.describe());
        Player player = new Player();
        entry.enter(player);

        Scanner scanner = new Scanner(System.in);
        String command;
        System.out.println("> ");

        // Wait for the enter key to be pressed
        while (!((command = scanner.nextLine()) == null)) {
            // Push input into command line
            CmdLine cmdLine = new CmdLine(command);
            // Check if any command has been entered
            for (Command cmd : commands)
                if (cmd.matches(cmdLine)) {
                    cmd.handle(cmdLine, player);
                    break;
                }
            System.out.println("> ");
        }
    }


    /**
     * Build a house and enter it
     * @param args
     */
    public static void main(String[] args) {
        House house = new House();
        house.enter();
    }


}
