package jdp.abstractfactory.game;

public interface Command {
    /**
     * Check the commands against a specific input
     * @param cmdLine- the input
     * @return true if input matches a command
     */
    public boolean matches(CmdLine cmdLine);

    /**
     * Execute the command
     * @param cmdLine- the command
     * @param player- the player
     */
    public void handle(CmdLine cmdLine, Player player);
}



