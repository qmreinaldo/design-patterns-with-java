package jdp.abstractfactory.game;

public class ComponentFactory {
    /**
     * Build a room
     * @param name
     * @param description
     * @return the room
     */
    Room createRoom(String name, String description) {
        return new Room(name, description);
    }

    /**
     * Build a door
     * @param room1 from
     * @param room2 to
     * @return the door
     */
    Door createDoor(Room room1, Room room2) {
        return new Door(room1, room2);
    }

    /**
     * Build a wall
     * @return the wall
     */
    Wall createWall() {
        return new Wall();
    }
}
