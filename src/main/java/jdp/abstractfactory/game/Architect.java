package jdp.abstractfactory.game;

public class Architect {
    /*
    The architect needs a factory
     */
    private final ComponentFactory factory;

    /**
     * Constructor: Get the factory
     * @param factory
     */
    Architect(ComponentFactory factory) {
        this.factory = factory;
    }

    /**
     * Build the house, creates rooms, set doors
     * @return Entry room
     */
    Room buildHouse() {
        Room hallway = factory.createRoom("hallway", "in the hallway");
        Room corridor = factory.createRoom("corridor", "in the corridor");
        Room bathroom = factory.createRoom("bathroom", "in the bathroom");
        Room study = factory.createRoom("study", "in the study");
        Room livingroom  = factory.createRoom("living room", "in the living room");
        Room kitchen = factory.createRoom("kitchen", "in the kitchen");
        Room pantry = factory.createRoom("pantry", "in the pantry");
        Room library = factory.createRoom("library", "in the library");

        // Specify the doors between the rooms
        Door[] door = new Door[7];
        door[0] = factory.createDoor(hallway, corridor);
        door[1] = factory.createDoor(hallway, bathroom);
        door[2] = factory.createDoor(corridor, library);
        door[3] = factory.createDoor(corridor, kitchen);
        door[4] = factory.createDoor(kitchen, pantry);
        door[5] = factory.createDoor(corridor, livingroom);
        door[6] = factory.createDoor(livingroom, study);

        // Places the doors
        hallway.addComponent(Direction.NORTH, door[0]);
        hallway.addComponent(Direction.WEST, door[1]);
        hallway.addComponent(Direction.NORTH, door[2]);
        hallway.addComponent(Direction.EAST, door[3]);
        hallway.addComponent(Direction.NORTH, door[4]);
        hallway.addComponent(Direction.WEST, door[5]);
        hallway.addComponent(Direction.NORTH, door[6]);

        return hallway;
    }
}
