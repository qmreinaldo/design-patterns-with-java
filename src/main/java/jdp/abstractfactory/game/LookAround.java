package jdp.abstractfactory.game;

public class LookAround implements Command {

    /**
     * The command is simply "look"
     * @param cmdLine- the input
     * @return result of the check
     */
    @Override
    public boolean matches(CmdLine cmdLine) {
        return cmdLine.startsWith("look");
    }

    /**
     * Describe the current room
     * @param cmdLine- the command
     * @param player- the player
     */

    @Override
    public void handle(CmdLine cmdLine, Player player) {
        System.out.println(player.getCurrentRoom().describe());
    }
}
