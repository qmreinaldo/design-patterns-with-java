package jdp.abstractfactory.game;

public enum Direction {
    NORTH("to the North"),
    SOUTH("to the South"),
    WEST("to the West"),
    EAST("to the East"),
    UNKNOWN("unknown direction");

    /*
    The current direction
     */
    private final String description;

    private Direction oppositeDirection;

    private Direction(String description) {
        this.description = description;
    }

    private void setOppositeDirection(Direction oppositeDirection) {
        this.oppositeDirection = oppositeDirection;
    }

    /**
     * Specify the opposite direction for every direction
     */
    static {
        NORTH.setOppositeDirection(SOUTH);
        SOUTH.setOppositeDirection(NORTH);
        EAST.setOppositeDirection(WEST);
        WEST.setOppositeDirection(EAST);
    }

    Direction getOppositeDirection() {
        return oppositeDirection;
    }

    /**
     * Convert text commands into directions
     * @param direction - the text command
     * @return the corresponding direction
     */
    static Direction getDirection(String direction) {
        direction = direction.toLowerCase();
        return switch (direction) {
            case "south" -> SOUTH;
            case "north" -> NORTH;
            case "east" -> EAST;
            case "west" -> WEST;
            default -> UNKNOWN;
        };
    }

    @Override
    public String toString() {
        return description;
    }
}
