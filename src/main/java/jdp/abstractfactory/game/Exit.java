package jdp.abstractfactory.game;

public class Exit implements Command {

    /**
     * This command is simple "quit"
     * @param cmdLine- the input
     * @return
     */
    @Override
    public boolean matches(CmdLine cmdLine) {
        return cmdLine.startsWith("exit");
    }

    /**
     * Quit the application
     * @param cmdLine- the command
     * @param player- the player
     */

    @Override
    public void handle(CmdLine cmdLine, Player player) {
        System.exit(0);
    }
}
