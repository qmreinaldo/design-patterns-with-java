package jdp.abstractfactory.game;

import java.util.EnumMap;

public class Room extends Component {
    /*
    The description of a room
     */
    private final String description;

    /*
    Name of the room
     */
    private final String name;

    /*
    The components adjacent to this room in all directions
     */

    private final EnumMap<Direction, Component> directions = new EnumMap<>(Direction.class);


    /**
     * Creates a room
     * @param name of the room
     * @param description of the room
     */
    Room(String name, String description) {
        this.name = name;
        this.description = description;
        // At the start, put a wall in every direction
        for (Direction direction : Direction.values())
            directions.put(direction, new Wall());
    }

    /**
     * Place a wall at a specific direction of this room
     * @param direction
     * @param wall
     */
    void addComponent(Direction direction, Wall wall) {
        if (directions.get(direction) == null)
            directions.put(direction, wall);
        else
            System.out.println("Not possible to place a wall in this direction: "
                            + name + " " + direction + " " + directions.get(direction));
    }

    /**
     * Place a door at a specific direction
     * @param direction
     * @param door
     */
    void addComponent(Direction direction, Door door) {
        // First get the opposite direction
        Direction oppositeDirection = direction.getOppositeDirection();

        // Check which rooms should be connected
        Room neighbor = door.getNeighbor(this);

        // What component do we have currently placed into this direction
        Component mySide = directions.get(direction);

        // Find out which component the neighbor has towards us
        Component opposingSide = neighbor.directions.get(oppositeDirection);

        // We only can place a door if there is both walls currently placed
        if (opposingSide instanceof Wall && mySide instanceof Wall) {
            // Place the door at the own room
            directions.put(direction, door);

            // Place the door at the neighbour respective position
            neighbor.directions.put(oppositeDirection, door);
        } else
            System.out.println("A door cannot be placed here.");
    }

    /**
     * Look for the component in a specific direction of the room
     * @param direction
     * @return
     */
    Component getComponentAt(Direction direction) {
        return directions.get(direction);
    }

    /**
     * Enter the room
     * @param player- who enters the room
     */
    @Override
    protected void enter(Player player) {
        System.out.println("You are now located " + description);
        player.setCurrentRoom(this);
    }

    /**
     * Describe a room and look at all directions
     * @return the descriptive text
     */
    @Override
    protected String describe() {
        String ans = "\nYou are " + description + "."
                + "\n You see \n";
        for (Direction direction : Direction.values()) {
            if (!(direction == Direction.UNKNOWN))
                ans += "\t- " + direction + " : " + directions.get(direction) + "\n";
        }
        return ans;
    }

    @Override
    public String toString() {
        return name;
    }
}
