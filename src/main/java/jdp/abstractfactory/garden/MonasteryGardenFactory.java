package jdp.abstractfactory.garden;

public class MonasteryGardenFactory implements AbstractGardenFactory {

    /**
     * Plant herbs
     * @return
     */
    @Override
    public Plant plant() {
        return new Herb();
    }
}
