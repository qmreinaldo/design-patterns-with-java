package jdp.abstractfactory.garden;

public interface AbstractGardenFactory {
    /**
     * Abstract action: plant
     * @return the Plant
     */
    Plant plant();

}
