package jdp.abstractfactory.garden;

public class Gardening {
    /**
     * Let a garden factory execute all the necessary steps
     */
    Gardening() {
        AbstractGardenFactory factory = new MonasteryGardenFactory();
        Plant plant = factory.plant();
    }

    public static void main(String[] args) {
        Gardening gardering = new Gardening();
    }
}
