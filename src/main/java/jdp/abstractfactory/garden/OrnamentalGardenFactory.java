package jdp.abstractfactory.garden;

public class OrnamentalGardenFactory implements AbstractGardenFactory {

    @Override
    public Plant plant() {
        return new Rose();
    }
}
