package jdp.proxy;

import jdp.proxy.cachingproxy.YouTubeCacheProxy;
import jdp.proxy.downloader.YouTubeDownloader;
import jdp.proxy.medialibrary.ThirdPartyYouTubeClass;

public class Demo {
    public static void main(String[] args) {
        YouTubeDownloader naiveDownloader = new YouTubeDownloader(new ThirdPartyYouTubeClass());
        YouTubeDownloader smartDownloader = new YouTubeDownloader(new YouTubeCacheProxy());

        long naive = test(naiveDownloader);
        long smart = test(smartDownloader);
        System.out.println("Time saved by proxy: " + (naive - smart) + " ms");
    }

    private static long test(YouTubeDownloader downloader) {
        long startTime = System.currentTimeMillis();

        downloader.renderPopularVideo();
        downloader.renderVideoPage("xyz123");
        downloader.renderPopularVideo();
        downloader.renderVideoPage("asd456");

        downloader.renderVideoPage("xyz123");
        downloader.renderVideoPage("asd456");

        long estimatedTime = System.currentTimeMillis()-startTime;
        System.out.println("Time elapsed: " + estimatedTime + "ms\n");
        return estimatedTime;
    }
}
