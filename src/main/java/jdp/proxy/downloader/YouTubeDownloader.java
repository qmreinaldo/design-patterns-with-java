package jdp.proxy.downloader;

import jdp.proxy.medialibrary.ThirdPartyYouTubeLibrary;
import jdp.proxy.medialibrary.Video;

import java.util.HashMap;

public class YouTubeDownloader {
    private ThirdPartyYouTubeLibrary api;

    public YouTubeDownloader(ThirdPartyYouTubeLibrary api) {
        this.api = api;
    }

    public void renderVideoPage(String videoId) {
        Video video = api.getVideo(videoId);
        System.out.println("\nVideo page ");
        System.out.println("ID: " + video.id);
        System.out.println("Title: " + video.title);
        System.out.println("Video: " + video.data);
    }

    public void renderPopularVideo() {
        HashMap<String, Video> list = api.popularVideos();
        System.out.println("Most popular videos on YouTube");
        for (Video video : list.values())
            System.out.println("ID: " + video.id + " Title: " + video.title);
    }
}
