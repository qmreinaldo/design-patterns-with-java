package jdp.proxy.medialibrary;

import java.util.HashMap;
import java.util.Vector;

public class ThirdPartyYouTubeClass implements ThirdPartyYouTubeLibrary {
    @Override
    public HashMap<String, Video> popularVideos() {
        connectToServer("http://www.youtube.com");
        return getRandomVideos();
    }

    @Override
    public Video getVideo(String videoId) {
        connectToServer("http://www.youtube.com/" + videoId);
        return getSomeVideo(videoId);
    }

    private int random(int min, int max) {
        return min + (int) (Math.random() * (max - min) +1);
    }

    private void experienceNetworkLatency() {
        int randomLatency = random(5, 10);
        for (int i = 0; i < randomLatency; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }

    private void connectToServer(String server) {
        System.out.println("Connecting to" + server + "...");
        experienceNetworkLatency();
        System.out.println("Connected!\n");
    }

    private HashMap<String, Video> getRandomVideos() {
        System.out.println("Downloading popular videos");

        experienceNetworkLatency();
        HashMap<String, Video> hashMap = new HashMap<String, Video>();
        hashMap.put("xyz123", new Video("abs123", "cats.mp4"));
        hashMap.put("asd456", new Video("uvw567", "Messi.mp4"));

        System.out.println("Done! + \n");
        return hashMap;
    }

    private Video getSomeVideo(String videoId) {
        System.out.println("Downloading video...");
        experienceNetworkLatency();
        Video video = new Video(videoId, "Some video title");

        System.out.println("Done! + \n");
        return video;
    }
}
