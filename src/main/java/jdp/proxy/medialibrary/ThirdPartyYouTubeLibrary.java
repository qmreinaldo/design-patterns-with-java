package jdp.proxy.medialibrary;

import java.util.HashMap;

public interface ThirdPartyYouTubeLibrary {
    HashMap<String, Video> popularVideos();
    Video getVideo(String videoId);
}
