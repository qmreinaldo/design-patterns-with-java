package jdp.proxy.cachingproxy;

import jdp.proxy.medialibrary.ThirdPartyYouTubeClass;
import jdp.proxy.medialibrary.ThirdPartyYouTubeLibrary;
import jdp.proxy.medialibrary.Video;

import java.util.HashMap;

public class YouTubeCacheProxy implements ThirdPartyYouTubeLibrary {
    private ThirdPartyYouTubeLibrary youTubeLibrary;
    private HashMap<String, Video> cachePopular = new HashMap<>();
    private HashMap<String, Video> cacheAll = new HashMap<>();

    public YouTubeCacheProxy() {
        this.youTubeLibrary = new ThirdPartyYouTubeClass();
    }

    @Override
    public HashMap<String, Video> popularVideos() {
        if (cachePopular.isEmpty())
            cachePopular = youTubeLibrary.popularVideos();
        else
            System.out.println("Retrieved list from cache");
        return cachePopular;
    }

    @Override
    public Video getVideo(String videoId) {
        Video video = cacheAll.get(videoId);
        if (video == null) {
            video = youTubeLibrary.getVideo(videoId);
            cacheAll.put(videoId, video);
        } else
            System.out.println("Retrieved video " + videoId + " from cache.");
        return video;
    }

    public void reset() {
        cachePopular.clear();
        cacheAll.clear();
    }
}
