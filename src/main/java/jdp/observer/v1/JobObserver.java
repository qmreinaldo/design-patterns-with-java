package jdp.observer.v1;

public interface JobObserver {
    void newOffer(String job);
}
