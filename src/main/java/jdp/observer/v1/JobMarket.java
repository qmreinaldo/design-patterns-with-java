package jdp.observer.v1;

import java.util.ArrayList;
import java.util.List;

public class JobMarket {
    private final List<JobObserver> observerList = new ArrayList<>();
    private final List<String> jobList = new ArrayList<>();

    public JobMarket() {

    }

    public void addJob(String job) {
        jobList.add(job);
        for (JobObserver jobObserver : observerList)
            jobObserver.newOffer(job);
    }

    public void addObserver(JobObserver jobObserver) {
        observerList.add(jobObserver);
    }

    public void removeObserver(JobObserver jobObserver) {
        observerList.remove(jobObserver);
    }
}
