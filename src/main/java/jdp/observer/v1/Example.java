package jdp.observer.v1;

public class Example {
    public static void main(String[] args) {
        JobMarket jobMarket = new JobMarket();

        JobObserver student = new Student("Pepe");
        JobObserver employee = new Employee("Juan");

        jobMarket.addObserver(student);
        jobMarket.addObserver(employee);

        jobMarket.addJob("developer");
    }
}
