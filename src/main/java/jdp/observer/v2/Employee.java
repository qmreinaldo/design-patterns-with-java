package jdp.observer.v2;

public class Employee implements JobObserver{
    private final String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public void newOffer(String job) {
        var randomNumber = (int) (Math.random() * 10);
        var answer = "Employee " + name;
        if (randomNumber <= 8)
            answer = answer + " applies for the job";
        else
            answer = answer + " does not apply";
        System.out.println(answer);
    }
}
