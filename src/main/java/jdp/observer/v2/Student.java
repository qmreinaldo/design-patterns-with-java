package jdp.observer.v2;

public class Student implements JobObserver{
    private final String name;

    public Student(String name) {
        this.name = name;
    }

    @Override
    public void newOffer(String job) {
        var randomNumber = (int) (Math.random()*10);
        var answer = "Student " + name;
        if (randomNumber <= 5)
            answer = answer + " applies for the job";
        else
            answer = answer + " does not apply";
        System.out.println(answer);
    }
}
