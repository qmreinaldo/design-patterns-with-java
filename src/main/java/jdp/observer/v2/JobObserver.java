package jdp.observer.v2;

public interface JobObserver {
    void newOffer(String job);
}
