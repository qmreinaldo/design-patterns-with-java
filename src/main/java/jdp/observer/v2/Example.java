package jdp.observer.v2;

public class Example {
    public static void main(String[] args) {
        var jobMarket = new JobMarket();
        var employmentAgency = new EmploymentAgency();
        var providers = new JobProvider[] {jobMarket, employmentAgency};

        var employee = new Employee("Pepe");
        var student = new Student("Adriana");

        jobMarket.addObserver(student);
        employmentAgency.addObserver(student);

        var job = "Developer";
        System.out.println("New Job: " + job);
        jobMarket.addJob(job);

        job = "QA";
        System.out.println("New job: " + job);
        for (JobProvider jobProvider : providers)
            jobProvider.addJob(job);

        jobMarket.addObserver(employee);

        job = "NLP engineer";
        System.out.println("New job: " + job);
        jobMarket.addJob(job);

        jobMarket.removeObserver(employee);

        job = "Project Manager";
        System.out.println("New Job: " + job);
        employmentAgency.addJob(job);
    }
}