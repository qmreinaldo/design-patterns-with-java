package jdp.observer.v2;

import java.util.ArrayList;
import java.util.List;

public class EmploymentAgency implements JobProvider{

    private final List<JobObserver> observerList = new ArrayList<>();
    private final List<String> jobOfferList = new ArrayList<>();
    @Override
    public void addJob(String job) {
        jobOfferList.add(job);
        for (JobObserver jobObserver : observerList)
            jobObserver.newOffer(job);
    }

    @Override
    public void addObserver(JobObserver jobObserver) {
        observerList.add(jobObserver);
    }

    @Override
    public void removeObserver(JobObserver jobObserver) {
        observerList.remove(jobObserver);
    }
}
