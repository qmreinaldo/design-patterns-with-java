package jdp.observer.v2;

import java.util.ArrayList;
import java.util.List;

public class JobMarket implements JobProvider {
    private final List<JobObserver> observerList = new ArrayList<>();
    private final List<String> jobOfferList = new ArrayList<>();


    @Override
    public void addJob(String job) {
        jobOfferList.add(job);
        for (JobObserver jobObserver : observerList)
            jobObserver.newOffer(job);  // Notify all observers.
    }

    @Override
    public void addObserver(JobObserver jobObserver) {
        observerList.add(jobObserver);
    }

    @Override
    public void removeObserver(JobObserver jobObserver) {
        observerList.remove(jobObserver);
    }
}
