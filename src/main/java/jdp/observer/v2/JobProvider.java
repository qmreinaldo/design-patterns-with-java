package jdp.observer.v2;

public interface JobProvider {
    void addJob(String job);
    void addObserver(JobObserver jobObserver);
    void removeObserver(JobObserver jobObserver);
}
