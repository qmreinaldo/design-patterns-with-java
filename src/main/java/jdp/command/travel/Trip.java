package jdp.command.travel;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Trip {
    final String destination;
    final String from;
    final String to;

    final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    Trip(String destination, LocalDate from, LocalDate to) {
        this.destination = destination;
        this.from = from.format(dateTimeFormatter);
        this.to = dateTimeFormatter.format(to);
    }

    @Override
    public String toString() {
        return "Travel to " + destination + " from " + from + " to  " + to;
    }
}
