package jdp.command.travel;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


import static java.time.temporal.TemporalAdjusters.nextOrSame;


public class Example {
    public static void main(String[] args) {
        var operator = new TourOperator("Operator123");
        var travelAgency = new TravelAgency(operator);
        LocalDate from, to;

        // book a trip
        from = LocalDate.of(2023, Month.NOVEMBER, 4);
        to = from.withDayOfMonth(15);   // Until the fifteenth of the same month
        travelAgency.bookTrip("Washington", from, to);

        // book another trip
        from = toDate("12/30/2023");
        to = from.with(nextOrSame(DayOfWeek.TUESDAY));  // Until the next upcoming tuesday
        travelAgency.bookTrip("Rom", from, to);

        // book a last trip
        from = toDate("10/02/2023");
        to = from.plusWeeks(2);  // Duration of exactly 2 weeks
        travelAgency.bookTrip("Peking", from, to);
    }

    private static LocalDate toDate(String date) {
        LocalDate tempDate;
        try {
            tempDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        }
        catch (DateTimeParseException exception) {
            tempDate = null;
            exception.printStackTrace();
        }
        return tempDate;
    }
}
