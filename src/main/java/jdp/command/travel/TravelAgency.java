package jdp.command.travel;

import java.time.LocalDate;

public class TravelAgency {
    private final TourOperator operator;
    TravelAgency(TourOperator operator) {
        this.operator = operator;
    }

    void bookTrip(String destination, LocalDate from, LocalDate to) {
        var trip = new Trip(destination, from, to);
        operator.execute(trip);
    }
}
