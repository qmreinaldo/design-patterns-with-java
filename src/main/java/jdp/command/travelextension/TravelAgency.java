package jdp.command.travelextension;

import java.time.LocalDate;

public class TravelAgency implements Provider {
    /*
    The command is implemented in a separate object
     */
    private final TripCommand tripCommand;
    TravelAgency(TripCommand tripCommand) {
        this.tripCommand = tripCommand;
    }

    @Override
    public void bookTrip(String destination, LocalDate from, LocalDate to) {
        tripCommand.book(destination, from, to);
    }
}
