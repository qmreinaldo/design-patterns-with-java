package jdp.command.travelextension;

public class TourOperator {
    private final String company;

    TourOperator (String company) {
        this.company = company;
    }

    void execute(Trip trip) {
        System.out.println(company + " operates the following trip " + trip);
    }

    @Override
    public String toString() {
        return company;
    }
}
