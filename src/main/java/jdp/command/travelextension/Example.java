package jdp.command.travelextension;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;



public class Example {
    public static void main(String[] args) {
        var operator123 = new TourOperator("Operator123");
        var operatorABC = new TourOperator("OperatorABC");

        TripCommand tripCommandABC = new TripCommand(operatorABC);
        TripCommand tripCommand123 = new TripCommand(operator123);

        Provider agency = new TravelAgency(tripCommand123);
        Provider branch = new Branch(tripCommandABC);

        LocalDate from, to;

        from = toDate("08/01/2023");
        to = toDate("08/15/2023");
        agency.bookTrip("Cairo", from, to);

        from = toDate("07/14/2023");
        to = toDate("07/15/2023");
        branch.bookTrip("Paris", from, to);

        from = toDate("10/02/2023");
        to = toDate("10/04/2023");
        agency.bookTrip("Berlin", from, to);

    }

    /**
     * Covert a text to a LocalDate
     * @param date - as text in format MM/dd/yyyy
     * @return date as java.time.LocalDate
     */
    private static LocalDate toDate(String date) {
        LocalDate tempDate;
        try {
            tempDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        }
        catch (DateTimeParseException exception) {
            tempDate = null;
            exception.printStackTrace();
        }
        return tempDate;
    }
}
