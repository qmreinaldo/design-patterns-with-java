package jdp.command.travelextension;

import java.time.LocalDate;

public class TripCommand {
    /*
    Reference to the operator
     */
    private final TourOperator operator;

    public TripCommand(TourOperator operator) {
        this.operator = operator;
    }

    public void book(String destination, LocalDate from, LocalDate to) {
        Trip trip = new Trip(destination, from, to);
        operator.execute(trip);
    }
}
