package jdp.command.travelextension;

import java.time.LocalDate;

public interface Provider {
    void bookTrip(String destination, LocalDate from, LocalDate to);
}
