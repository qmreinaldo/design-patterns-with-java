package jdp.builder.travel;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Trip {
    /*
    Inner class for building a trip
     */
    public static class Builder {
        private LocalDate startDate ;
        private LocalDate endDate;
        private int duration;
        private int numberOfTravellers;
        private int numberOfKids = 0;
        private int minimumStars = 0;
        private int minimumRecommendations = 0;
        private int rating = 0;
        private int minimumNumberOfRatings = 0;

        /**
         * Constructor: It gets the basic properties
         * @param startDate
         * @param endDate
         * @param duration
         * @param numberOfTravellers
         */
        public Builder(LocalDate startDate, LocalDate endDate, int duration, int numberOfTravellers) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.duration = duration;
            this.numberOfTravellers = numberOfTravellers;
        }

        /**
         * Set the number of children
         * @param numberOfKids
         * @return Builder again
         */
        public Builder numberOfKids(int numberOfKids) {
            this.numberOfKids = numberOfKids;
            return this;
        }

        /**
         * Set the minimum number of stars required
         * @param minimumStars
         * @return Builder again
         */
        public Builder minimumStars(int minimumStars) {
            this.minimumStars = minimumStars;
            return this;
        }

        /**
         * Set recommendations rate
         * @param minimumRecommendations
         * @return Builder again
         */
        public Builder minimumRecommendations(int minimumRecommendations) {
            this.minimumRecommendations = minimumRecommendations;
            return this;
        }

        /**
         * Set recommendations rate
         * @param rating
         * @return Builder again
         */
        public Builder rating(int rating) {
            this.rating = rating;
            return this;
        }

        /**
         * Set minimum number of ratings
         * @param minimumNumberOfRatings
         * @return Builder again
         */
        public Builder minimumNumberRatings(int minimumNumberOfRatings) {
            this.minimumNumberOfRatings = minimumNumberOfRatings;
            return this;
        }

        /**
         * It calls the constructor for the Trip
         * @return the new Trip
         */
        public Trip build() {
            return new Trip(this);
        }
    }

    private final LocalDate startDate ;
    private final LocalDate endDate;

    /*
    Starting date as a text
     */
    public final String start;

    /*
    End date as a text
     */
    public final String end;
    private final int duration;
    private final int numberOfTravellers;
    private final int numberOfKids;
    private final int minimumStars;
    private final int minimumRecommendations;
    private final int rating;
    private final int minimumNumberOfRatings;

    /**
     * Constructor. Gets everything of the given Builder
     * @param builder with every property of the trip
     */
    private Trip(Builder builder) {
        this.startDate = builder.startDate;
        this.endDate = builder.endDate;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
        start = formatter.format(startDate);
        end = formatter.format(endDate);
        this.duration = builder.duration;
        this.numberOfTravellers = builder.numberOfTravellers;
        this.numberOfKids = builder.numberOfKids;
        this.minimumStars = builder.minimumStars;
        this.minimumRecommendations = builder.minimumRecommendations;
        this.rating = builder.rating;
        this.minimumNumberOfRatings = builder.minimumNumberOfRatings;
    }

    /**
     * Creates and return a static Trip-Builder with some basic data.
     * Eligible to be called from static methods.
     * @param startDate
     * @param endDate
     * @param duration
     * @param numberOfTravellers
     * @return the builder
     */
    public static Trip.Builder builder (LocalDate startDate, LocalDate endDate, int duration, int numberOfTravellers) {
        return new Trip.Builder(startDate, endDate, duration, numberOfTravellers);
    }

    @Override
    public String toString() {
        return "Trip{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", duration=" + duration +
                ", numberOfTravellers=" + numberOfTravellers +
                ", numberOfKids=" + numberOfKids +
                ", minimumStars=" + minimumStars +
                ", minimumRecommendations=" + minimumRecommendations +
                ", rating=" + rating +
                ", minimumNumberOfRatings=" + minimumNumberOfRatings +
                '}';
    }
}
