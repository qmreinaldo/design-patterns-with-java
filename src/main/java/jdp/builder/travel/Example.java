package jdp.builder.travel;

import java.time.LocalDate;

public class Example {
    public static void main(String[] args) {
        Trip trip = Trip.builder(LocalDate.now(), LocalDate.now().plusDays(15), 7, 2)
                .minimumStars(3)
                .rating(5)
                .numberOfKids(0)
                .build();
        System.out.println(trip);
    }
}
