package jdp.strategy;

public class QuickSortStrategy implements SortStrategy{
    @Override
    public void sort(int[] numbers) {
        quicksort(numbers, 0, numbers.length-1);
    }

    private static void quicksort(int[] A, int left, int right) {
        if (left >= right)
            return;
        int pivot = left+(right-left)/2;
        int l = left, r = right;
        while (l < r) {
            while (A[l] < A[pivot])
                l++;
            while (A[r] > A[pivot])
                r--;
            swap(A, l, r);
            l++;
            r--;
        }
        quicksort(A, left, pivot-1);
        quicksort(A, pivot+1, right);
    }

    private static void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}




