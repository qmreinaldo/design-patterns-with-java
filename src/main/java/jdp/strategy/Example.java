package jdp.strategy;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;

public class Example {
    public static void testSorting() {
        int size = 100000;
        int range = 100000;

        SortStrategy sortStrategy;
        int[] numbers1 = createArray(size, range);
        int[] numbers2 = numbers1.clone();
        int[] numbers3 = numbers1.clone();

        sortStrategy = new SelectionSortStrategy();
        executeStrategy(sortStrategy, numbers1);

        sortStrategy = new MergeSortStrategy();
        executeStrategy(sortStrategy, numbers2);

        sortStrategy = new QuickSortStrategy();
        executeStrategy(sortStrategy, numbers3);
    }

    private static void executeStrategy(SortStrategy sortStrategy, int[] numbers) {
        Instant start;
        Instant end;
        Long difference;

        System.out.println(sortStrategy);

        start = Instant.now();
        sortStrategy.sort(numbers);
        end = Instant.now();

        Duration elapsed = Duration.between(start, end);
        difference = elapsed.toMillis();
        System.out.println(difference + " Milliseconds \n");
    }

    private static int[] createArray(int size, int range) {
        int[] result = new int[size];
        Random generator = new Random();

        for (int i = 0; i < size; i++)
            result[i] = generator.nextInt(range);
        return result;
    }

    public static void main(String[] args) {
        testSorting();
    }
}
