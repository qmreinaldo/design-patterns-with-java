package jdp.strategy;

public interface SortStrategy {
    void sort(int[] numbers);
}
