package jdp.strategy;

public class MergeSortStrategy implements SortStrategy {
    @Override
    public void sort(int[] numbers) {
        mergeSort(numbers, 0, numbers.length-1);
    }

    private static void mergeSort(int[] A, int left, int right) {
        if (left == right)
            return;
        int mid = left+(right-left)/2;
        mergeSort(A, left, mid);
        mergeSort(A, mid+1, right);
        merge(A, left, right);
    }

    private static void merge(int[] A, int left, int right) {
        int[] aux = new int[A.length];
        if (right + 1 - left >= 0) System.arraycopy(A, left, aux, left, right + 1 - left);
        int mid = left+(right-left)/2;
        int l = left, r = mid+1, index = left;
        while (l <= mid && r <= right) {
            if (aux[l] <= aux[r]) {
                A[index] = aux[l];
                l++;
            } else {
                A[index] = aux[r];
                r++;
            }
            index++;
        }
        for (int k = l; k <= mid; k++) {
            A[index] = aux[k];
            index++;
        }
        for (int k = r; k <= right; k++) {
            A[index] = aux[k];
            index++;
        }
    }
}
