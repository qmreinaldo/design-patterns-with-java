package jdp.strategy;

public class SelectionSortStrategy implements SortStrategy{

    @Override
    public void sort(int[] numbers) {
        int indexOfMin;
        for (int i = 0; i < numbers.length-1; i++) {
            indexOfMin = i;
            for (int j = i+1; j < numbers.length; j++) {
                if (numbers[indexOfMin] > numbers[j])
                    indexOfMin = j;
            }
            swap(numbers, indexOfMin, i);
        }
    }

    private static void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}
