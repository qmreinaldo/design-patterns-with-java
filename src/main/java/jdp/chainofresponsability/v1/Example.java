package jdp.chainofresponsability.v1;


public class Example {
    public static void main(String[] args) {
        // Create the merchants
        var farmshop = new FarmShop("Farm Shop");
        var bakery = new Bakery("Bakery");
        var liquorStore = new LiquorStore("Liquor Store");

        // Build up the chain, beginning with bakery
        bakery.setNext(liquorStore);
        liquorStore.setNext(farmshop);

        // Request a grocery
        bakery.sell(Groceries.CHEESE);
        bakery.sell(Groceries.EGGS);
        bakery.sell(Groceries.EGGS);
        bakery.sell(Groceries.BEER);
    }
}
