package jdp.chainofresponsability.v1;

public class FarmShop extends AbstractMerchant {
    private final String name;

    public FarmShop(String name) {
        this.name = name;
    }

    /**
     * When article is EGGS, CHEESE or SAUSAGES, it is sold, otherwise, the task is forwarded.
     * @param article
     */
    @Override
    public void sell(Groceries article) {
        if (article == Groceries.EGGS || article == Groceries.CHEESE || article == Groceries.SAUSAGES)
            System.out.println(name + " sells " + article);
        else
            super.forward(article);
    }
}
