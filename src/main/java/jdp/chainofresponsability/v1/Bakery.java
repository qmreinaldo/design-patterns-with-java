package jdp.chainofresponsability.v1;

public class Bakery extends AbstractMerchant {

    private final String name;

    public Bakery(String name) {
        this.name = name;
    }

    /**
     * When article is BREAD, it is sold, otherwise, the task is forwarded.
     * @param article
     */
    @Override
    public void sell(Groceries article) {
        if (article == Groceries.BREAD)
            System.out.println(name + " sells " + article);
        else
            super.forward(article);
    }
}
