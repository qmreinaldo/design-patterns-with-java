package jdp.chainofresponsability.v1;

public abstract class AbstractMerchant {
    private AbstractMerchant next;
    public abstract void sell (Groceries article);

    public void setNext(AbstractMerchant merchant) {
        if (next == null)
            next = merchant;
        else
            next.setNext(merchant);
    }

    // to pass the responsibility.
    protected void forward(Groceries article) {
        if (next != null)
            next.sell(article);
    }
}
