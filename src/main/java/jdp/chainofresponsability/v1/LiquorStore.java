package jdp.chainofresponsability.v1;

public class LiquorStore extends AbstractMerchant {
    private final String name;

    public LiquorStore(String name) {
        this.name = name;
    }

    /**
     * If article is BEER, it is sold, otherwise, the task is forwarded.
     * @param article
     */
    @Override
    public void sell(Groceries article) {
        if (article == Groceries.BEER)
            System.out.println(name + " sells " + article);
        else
            super.forward(article);
    }
}
