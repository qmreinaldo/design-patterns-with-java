package jdp.chainofresponsability.v2;

public class Bakery extends AbstractMerchant {

    private final String name;

    public Bakery(String name) {
        this.name = name;
    }

    /**
     * When purchase has article BREAD, it is sold,
     * otherwise, the task is forwarded.
     * @param purchase
     */
    @Override
    public void sell(Purchase purchase) {
        if (purchase.getArticle() == Groceries.BREAD)
            while (this.isAvailable() && purchase.stillDemanding()) {
                System.out.println(name + " sells " + purchase.getArticle());
                purchase.sellGood();
            }
        if (purchase.stillDemanding())
            super.forward(purchase);
    }

    private boolean isAvailable() {
        var random = Math.random();
        return random >= 0.5;
    }
}
