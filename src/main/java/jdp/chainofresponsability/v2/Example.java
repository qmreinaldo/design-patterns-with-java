package jdp.chainofresponsability.v2;


public class Example {
    public static void main(String[] args) {
        // Create the merchants
        var farmshop = new FarmShop("Farm Shop");
        var bakery = new Bakery("Bakery");
        var liquorStore = new LiquorStore("Liquor Store");

        // Build up the chain, beginning with bakery
        bakery.setNext(liquorStore);
        liquorStore.setNext(farmshop);

        // Request a purchase
        bakery.sell(new Purchase(Groceries.CHEESE, 3));
        bakery.sell(new Purchase(Groceries.EGGS, 4));
        bakery.sell(new Purchase(Groceries.BREAD, 1));
        bakery.sell(new Purchase(Groceries.BEER, 2));
    }
}
