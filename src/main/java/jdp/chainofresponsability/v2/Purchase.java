package jdp.chainofresponsability.v2;

public class Purchase {
    private final Groceries article;
    private int quantity;

    Purchase(Groceries article, int quantity) {
        this.article = article;
        this.quantity = quantity;
    }

    public Groceries getArticle() {
        return this.article;
    }

    public void sellGood() {
        quantity--;
    }

    public boolean stillDemanding() {
        return quantity > 0;
    }

    @Override
    public String toString() {
        return article.toString();
    }
}
