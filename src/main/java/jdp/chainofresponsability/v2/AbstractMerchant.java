package jdp.chainofresponsability.v2;

public abstract class AbstractMerchant {
    private AbstractMerchant next;
    public abstract void sell (Purchase purchase);

    public void setNext(AbstractMerchant merchant) {
        if (next == null)
            next = merchant;
        else
            next.setNext(merchant);
    }

    private void printMessage(Purchase purchase) {
        System.out.println("No merchant is able to sell " + purchase);
    }

    // to pass the responsibility.
    protected void forward(Purchase purchase) {
        if (next != null)
            next.sell(purchase);
        else                // No merchant available
            printMessage(purchase);
    }
}
