package jdp.chainofresponsability.v2;

public class LiquorStore extends AbstractMerchant {
    private final String name;

    public LiquorStore(String name) {
        this.name = name;
    }

    /**
     * If purchase has article BEER, it is sold, otherwise, the task is forwarded.
     * @param purchase
     */
    @Override
    public void sell(Purchase purchase) {
        if (purchase.getArticle() == Groceries.BEER)
            while (isAvailable() && purchase.stillDemanding()) {
                System.out.println(name + " sells " + purchase);
                purchase.sellGood();
            }

        if (purchase.stillDemanding())
            super.forward(purchase);
    }

    private boolean isAvailable() {
        var random = Math.random();
        return random >= 4;
    }
}
