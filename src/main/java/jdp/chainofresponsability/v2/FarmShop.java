package jdp.chainofresponsability.v2;

public class FarmShop extends AbstractMerchant {
    private final String name;

    public FarmShop(String name) {
        this.name = name;
    }

    /**
     * When purchase has article EGGS, CHEESE or SAUSAGES it is sold,
     * otherwise, the task is forwarded.
     * @param purchase
     */
    @Override
    public void sell(Purchase purchase) {
        if (purchase.getArticle() == Groceries.EGGS
                || purchase.getArticle() == Groceries.CHEESE
                || purchase.getArticle() == Groceries.SAUSAGES)
            while (this.isAvailable() && purchase.stillDemanding()) {
                System.out.println(name + " sells " + purchase);
                purchase.sellGood();
            }
        if (purchase.stillDemanding())
            super.forward(purchase);
    }

    private boolean isAvailable() {
        var random = Math.random();
        return random >= 0.6;
    }
}
