package jdp.decorator.basicmodels;

import jdp.decorator.commons.Component;

public class ModelB implements Component {

    /**
     * The price of the car
     * @return price
     */
    @Override
    public int getPrice() {
        return 19000;
    }

    /**
     * A short description of the car
     * @return text
     */
    @Override
    public String getDescription() {
        return "A medium sized car";
    }
}
