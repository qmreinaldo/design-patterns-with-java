package jdp.decorator.basicmodels;

import jdp.decorator.commons.Component;

public class ModelA implements Component {
    /**
     * The price of the car
     * @return price
     */
    @Override
    public int getPrice() {
        return 12000;
    }

    /**
     * A short description of the car
     * @return text
     */
    @Override
    public String getDescription() {
        return "A tiny car";
    }
}
