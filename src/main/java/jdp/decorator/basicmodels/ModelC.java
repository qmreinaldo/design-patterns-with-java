package jdp.decorator.basicmodels;

import jdp.decorator.commons.Component;

public class ModelC implements Component {

    /**
     * Returns the price of the car
     *
     * @return price
     */
    @Override
    public int getPrice() {
        return 24000;
    }

    /**
     * Returns the description of the car
     *
     * @return text
     */
    @Override
    public String getDescription() {
        return "A car of upper middle class";
    }
}
