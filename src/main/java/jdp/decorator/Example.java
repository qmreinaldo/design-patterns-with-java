package jdp.decorator;


import jdp.decorator.basicmodels.ModelB;
import jdp.decorator.basicmodels.ModelC;
import jdp.decorator.commons.Component;
import jdp.decorator.optionalequipment.AirConditioner;
import jdp.decorator.optionalequipment.GPS;

public class Example {
    public static void main(String[] args) {
        Component basicModel = new ModelC();
        Component gps = new GPS(basicModel);
        Component airConditioner = new AirConditioner(gps);

        System.out.println("Customer requirement: \n\t " +
                airConditioner.getDescription()
                + "\nPrice: \n\t" + airConditioner.getPrice());

    }
}
