package jdp.decorator.optionalequipment;

import jdp.decorator.commons.Component;

public class GPS extends OptionalEquipment {

    /**
     * Constructor. It gets a component and extends it with GPS
     * @param component the input component
     */
    public GPS(Component component) {
        super(component);
    }

    /**
     * Returns the price of the Component (added up)
     *
     * @return price
     */
    @Override
    public int getPrice() {
        return basicComponent.getPrice()+300;
    }

    /**
     * Returns a concatenated description
     *
     * @return text
     */
    @Override
    public String getDescription() {
        return basicComponent.getDescription() + " ans GPS";
    }
}
