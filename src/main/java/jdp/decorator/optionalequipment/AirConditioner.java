package jdp.decorator.optionalequipment;

import jdp.decorator.commons.Component;

public class AirConditioner extends OptionalEquipment {
    /**
     * Constructor. It gets a component and extends it with an AC
     * @param component the input component
     */
    public AirConditioner(Component component) {
        super(component);
    }


    /**
     * Returns the price of the Component (added up)
     *
     * @return price
     */
    @Override
    public int getPrice() {
        return basicComponent.getPrice()+500;
    }

    /**
     * Returns a concatenated description
     *
     * @return text
     */
    @Override
    public String getDescription() {
        return basicComponent.getDescription()+" and air conditioning";
    }
}
