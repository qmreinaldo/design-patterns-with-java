package jdp.decorator.optionalequipment;

import jdp.decorator.commons.Component;

public abstract class OptionalEquipment implements Component {
    /*
    A basic component which is extended with this component
     */
    protected final Component basicComponent;

    protected OptionalEquipment(Component component) {
        this.basicComponent = component;
    }

}
