package jdp.decorator.commons;

public interface Component {
    /**
     * Returns the price of a component
     * @return price
     */
    public int getPrice();

    /**
     * Returns the description of a component
     * @return text
     */
    public String getDescription();
}
