package jdp.visitor;

import jdp.visitor.car.Car;
import jdp.visitor.visitor.DiagnoseVisitor;
import jdp.visitor.visitor.PartsVisitor;
import jdp.visitor.visitor.VisitorIF;

public class Example {
    public static void main(String[] args) {
        Car car = new Car();
        PartsVisitor visitor = new PartsVisitor();
        car.accept(visitor);

        String parts = visitor.getPartsList();
        System.out.println(parts);

        DiagnoseVisitor diagnose = new DiagnoseVisitor();
        car.accept(diagnose);
        boolean isReady = diagnose.isOk();
        System.out.println("\nThe car is " + (isReady ? "ready" : "not ready"));
    }
}
