package jdp.visitor.car;

public enum Position {
    FL("front left"),
    FR("front right"),
    RL("rear left"),
    RR("rear right");

    private final String description;

    private Position(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
