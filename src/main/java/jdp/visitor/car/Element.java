package jdp.visitor.car;

import jdp.visitor.visitor.VisitorIF;

public interface Element {
    /**
     * Each part can deliver a describer of itself
     * @return text
     */
    String getDescription();

    /**
     * Each part can accept a visitor
     * @param visitor
     */
    void accept(VisitorIF visitor);

}

