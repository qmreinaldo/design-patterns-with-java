package jdp.visitor.car;

import jdp.visitor.visitor.VisitorIF;

public class Chassis implements Element {
    /*
    An array with all the wheels
     */
    private final Wheel[] wheels;

    Chassis(Wheel[] wheels) {
        this.wheels = wheels;
    }

    /**
     * An array with all the wheels
     * @return the array
     */
    public Wheel[] getWheels() {
        return wheels;
    }

    @Override
    public String getDescription() {
        return "-Chassis ";
    }

    /**
     * It gets a visitor and calls its visit method with itself as a parameter.
     * Then, the visitor is passed to the wheels.
     * @param visitor
     */
    @Override
    public void accept(VisitorIF visitor) {
        visitor.visit(this);
        for (Wheel wheel : wheels)
            wheel.accept(visitor);
    }

    @Override
    public String toString() {
        return "Chassis";
    }
}
