package jdp.visitor.car;

import jdp.visitor.visitor.VisitorIF;



public class Wheel implements Element {
    /*
    The position of the wheel
     */
    private final Position position;

    /*
    The pressure
     */
    private final double pressure = 2.0;

    public Wheel(Position position) {
        this.position = position;
    }
    /**
     * Get the pressure
     * @return a double value
     */
    public double getPressure() {
        return pressure;
    }

    /**
     * Print out the description
     * @return text
     */
    @Override
    public String getDescription() {
        return "-Wheel " + position;
    }

    /**
     * Take a visitor and call its visit method with the wheel as parameter
     * @param visitor
     */
    @Override
    public void accept(VisitorIF visitor) {
        visitor.visit(this);
    }

    /**
     * Print out the name
     * @return text
     */
    @Override
    public String toString() {
        return "Wheel " + position;
    }
}
