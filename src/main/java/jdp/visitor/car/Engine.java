package jdp.visitor.car;

import jdp.visitor.visitor.VisitorIF;

public class Engine implements Element {
    private final String status = "OK";

    public String getStatus() {
        return status;
    }

    @Override
    public String getDescription() {
        return "-Engine ";
    }

    /**
     * It takes a visitor and calls its visit method with the engine as parameter
     * @param visitor
     */
    @Override
    public void accept(VisitorIF visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "Engine";
    }
}


