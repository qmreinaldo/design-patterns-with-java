package jdp.visitor.car;


import java.util.ArrayList;
import java.util.List;

import jdp.visitor.visitor.VisitorIF;
public class Car implements Element {
    private final List<Element> parts = new ArrayList<>();

    private final boolean tankIsFull = true;

    /**
     * Constructor. Adds an engine, a chassis and four wheels.
     */
    public Car() {
        parts.add(new Engine());
        parts.add(new Chassis(new Wheel[]{
                new Wheel(Position.FL), new Wheel(Position.FR),
                new Wheel(Position.RL), new Wheel(Position.RR)
        }));
    }

    public List<Element> getParts() {
        return parts;
    }

    public boolean isTankIsFull() {
        return tankIsFull;
    }
    @Override
    public String getDescription() {
        return "-Car ";
    }

    /**
     * It gets a visitor and forwards it to all parts, only then the visitor to the car object
     * itself is called
     * @param visitor
     */
    @Override
    public void accept(VisitorIF visitor) {
        parts.forEach((part) -> {
            part.accept(visitor);
        });
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "Car";
    }
}
