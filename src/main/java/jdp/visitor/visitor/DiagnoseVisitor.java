package jdp.visitor.visitor;

import jdp.visitor.car.Car;
import jdp.visitor.car.Chassis;
import jdp.visitor.car.Engine;
import jdp.visitor.car.Wheel;

public class DiagnoseVisitor implements VisitorIF {
    /*
    Flag ig the car is OK
     */
    private boolean isOk = true;

    /**
     * Check if the car is OK
     * @return true if the car is ready to start
     */
    public boolean isOk() {
        return isOk;
    }

    /**
     * Check the air pressure of a given wheel.
     * @param wheel
     */
    @Override
    public void visit(Wheel wheel) {
        double pressure = wheel.getPressure();
        System.out.println("Pressure " + wheel + ": " + pressure);
        System.out.println(pressure == 2.0 ? " is sufficient" : "is not sufficient");

        if (pressure != 2.0)
            isOk = false;
    }

    /**
     * Check status
     * @param engine
     */
    @Override
    public void visit(Engine engine) {
        String status = engine.getStatus();
        System.out.println(status);
    }

    /**
     * For a chassis, the attached wheels are counted.
     * A car need 4 of them
     * @param chassis
     */
    @Override
    public void visit(Chassis chassis) {
        Wheel[] wheels = chassis.getWheels();
        if (wheels.length != 4)
            isOk = false;
    }

    /**
     * Only check if the tank is full
     * @param car
     */
    @Override
    public void visit(Car car) {
        boolean tankIsFull = car.isTankIsFull();
        System.out.println("The car tank is"
                + (tankIsFull? " full" : " is not full"));
        isOk = tankIsFull;
    }
}
