package jdp.visitor.visitor;
import jdp.visitor.car.*;

public interface VisitorIF {
    /**
     * Visit a wheel
     * @param wheel
     */
    void visit(Wheel wheel);

    /**
     * Visit an engine
     * @param engine
     */
    void visit(Engine engine);

    /**
     * Visit a chassis
     * @param chassis
     */
    void visit(Chassis chassis);

    /**
     * Visit a car
     * @param car
     */
    void visit(Car car);
}

