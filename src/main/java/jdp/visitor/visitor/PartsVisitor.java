package jdp.visitor.visitor;

import jdp.visitor.car.Car;
import jdp.visitor.car.Chassis;
import jdp.visitor.car.Engine;
import jdp.visitor.car.Wheel;

public class PartsVisitor implements VisitorIF {
    /*
    For building the parts list
     */
    private final StringBuilder builder = new StringBuilder();

    /**
     * Deliver the pars list as one text
     * @return
     */
    public String getPartsList() {
        return "Components: \n" + builder.toString();
    }

    /**
     * Get the description of a wheel
     * @param wheel
     */
    @Override
    public void visit(Wheel wheel) {
        builder.append(wheel.getDescription()).append("\n");
    }

    /**
     * Get the description of an engine
     * @param engine
     */
    @Override
    public void visit(Engine engine) {
        builder.append(engine.getDescription()).append("\n");
    }

    /**
     * Get the description of a chassis
     * @param chassis
     */
    @Override
    public void visit(Chassis chassis) {
        builder.append(chassis.getDescription()).append("\n");
    }

    /**
     * Get th description of a car
     * @param car
     */
    @Override
    public void visit(Car car) {
        builder.append(car.getDescription())
                .append(" (remaining parts)")
                .append("\n");
    }
}
