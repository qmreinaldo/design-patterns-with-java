package jdp.composite.budget4;

public abstract class Node {
    protected final String description;

    public Node(String description) {
        this.description = description;
    }

    /**
     * Get the value of the node
     * @return the value
     */
    abstract double getValue();

    /**
     * Calculate the sum of amounts below the node
     */
    public abstract void calculateCache();

    public Node getChild(int index) {
        throw new RuntimeException("A leaf does not have any children.");
    }

    public int getNumberChildren() {
        return 0;
    }
}
