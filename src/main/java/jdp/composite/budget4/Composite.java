package jdp.composite.budget4;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Node {
    private final List<Node> children = new ArrayList<>();

    /*
    A sum on the node level
     */
    private double cache =  0.0;

    public Composite(String description) {
        super(description);
    }

    public void add(Node child) {
        children.add(child);
    }

    @Override
    public void calculateCache() {
        cache = 0;
        for (Node node : children) {
            node.calculateCache();;
            cache += node.getValue();
        }
    }

    /**
     * Return the calculated sum
     * @return
     */
    @Override
    public double getValue() {
        return cache;
    }

    @Override
    public Node getChild(int index) {
        return children.get(index);
    }

    @Override
    public int getNumberChildren() {
        return children.size();
    }

    @Override
    public String toString() {
        return description + " (Sum: " + cache + ")";
    }
}
