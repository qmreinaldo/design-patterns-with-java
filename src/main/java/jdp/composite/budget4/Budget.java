package jdp.composite.budget4;


public class Budget {
    public static void main(String[] args) {
        final var root = new Composite("Budget book");
        final var january = new Composite("January");
        final var february = new Composite("February");
        final var income = new Composite("Income");
        final var expenses = new Composite("Expenses");
        final var insurances = new Composite("Insurances");
        final var books = new Composite("Books");

        january.add(income);
        january.add(expenses);

        root.add(january);
        root.add(february);

        income.add(new Leaf("Main job", 2100.00, true));
        income.add(new Leaf("Side Job", 200.00, true));

        expenses.add(insurances);
        expenses.add(books);

        expenses.add(new Leaf("Rent", -600.00, true));
        insurances.add(new Leaf("Car", -50.00, true));
        insurances.add(new Leaf("Health Care", -100.00, true));
        books.add(new Leaf("Design Patterns", -30.00, true));
        books.add(new Leaf("Attack on Titan", -60.00, false));

        root.calculateCache();
        print(root, 0);
    }

    // DFS
    private static void print(Node node, int indentation) {
        for (int i = 0; i < indentation; i++)
            System.out.print("\t");

        System.out.println(node);

        for (int j = 0; j < node.getNumberChildren(); j++) {
            Node childNode = node.getChild(j);
            print(childNode, indentation+1);
        }
    }
}
