package jdp.composite.budget4;

public class Leaf extends Node {
    private final boolean required;

    private double amount = 0.0;

    public Leaf(String description, double amount, boolean required) {
        super(description);
        this.amount = amount;
        this.required = required;
    }

    /**
     * A leaf has no cache
     */
    @Override
    public void calculateCache() {

    }

    @Override
    public double getValue() {
        return amount;
    }

    @Override
    public String toString() {
        String prefix = required ? "(!)" : "()";
        return prefix + description + ": " + amount;
    }
}
