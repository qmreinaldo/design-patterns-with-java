package jdp.composite.budget3;

public class Leaf extends Node {
    private final boolean required;

    private double amount = 0.0;

    public Leaf(String description, double amount, boolean required) {
        super(description);
        this.amount = amount;
        this.required = required;
    }

    @Override
    public String toString() {
        String prefix = required ? "(!)" : "()";
        return prefix + description + amount;
    }
}
