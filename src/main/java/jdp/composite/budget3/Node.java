package jdp.composite.budget3;

public abstract class Node {
    protected final String description;

    public Node(String description) {
        this.description = description;
    }

    public Node getChild(int index) {
        throw new RuntimeException("A leaf does not have any children.");
    }

    public int getNumberChildren() {
        return 0;
    }
}
