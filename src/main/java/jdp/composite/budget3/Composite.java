package jdp.composite.budget3;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Node {
    private final List<Node> children = new ArrayList<>();

    public Composite(String description) {
        super(description);
    }

    public void add(Node child) {
        children.add(child);
    }

    @Override
    public Node getChild(int index) {
        return children.get(index);
    }

    @Override
    public int getNumberChildren() {
        return children.size();
    }

    @Override
    public String toString() {
        return description;
    }
}
