package jdp.composite.budget1;

public class Budget {
    public static void main(String[] args) {
        final Composite root = new Composite("Budget book");
        final Composite january = new Composite("January");
        final Composite february = new Composite("February");
        final Composite income = new Composite("Income");
        final Composite expenses = new Composite("Expenses");
        final Composite insurances = new Composite("Insurances");
        final Composite books = new Composite("Books");

        january.add(income);
        january.add(expenses);

        root.add(january);
        root.add(february);

        income.add(new Leaf("Main job", 2100.00, true));
        income.add(new Leaf("Side Job", 200.00, true));

        expenses.add(insurances);
        expenses.add(books);

        expenses.add(new Leaf("Rent", -600.00, true));
        insurances.add(new Leaf("Car", -50.00, true));
        insurances.add(new Leaf("Health Care", -100.00, true));
        books.add(new Leaf("Design Patterns", -30.00, true));
        books.add(new Leaf("Attack on Titan", -60.00, false));

        // Print the whole root
        root.print(0);

        // Just print a part
        System.out.println("\n\nJust the expenses: ");
        expenses.print(0);
    }
}
