package jdp.composite.budget1;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Node {
    private final List<Node> children = new ArrayList<>();

    public Composite(String description) {
        super(description);
    }

    /**
     * Add a new child to the list
     * @param child the new child
     */
    public void add(Node child) {
        children.add(child);
    }

    /**
     * Returns the child node at the given index
     * @param index to look at
     * @return child node at given index
     */
    public Node getChild(int index) {
        return children.get(index);
    }

    public int getNumberOfChildNodes() {
        return children.size();
    }

    @Override
    public void print(int indentation) {
        for (int i = 0; i < indentation; i++)
            System.out.print("-");
        System.out.println(this);
        children.forEach((Node node) -> {
            node.print(indentation+1);
        });
    }

    @Override
    public String toString() {
        return description;
    }
}
