package jdp.composite.budget1;

public abstract class Node {
    protected final String description;

    public Node(String description) {
        this.description = description;
    }

    public abstract void print(int indentation);
}
