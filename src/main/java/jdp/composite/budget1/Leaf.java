package jdp.composite.budget1;

import java.text.NumberFormat;

public class Leaf extends Node {
    /*
    A flag, if the expenses or the income is really necessary
     */
    private final boolean required;

    /*
    The amount (income positive, expenses negative)
     */
    private double amount = 0.0;

    public Leaf(String description, double amount, boolean required) {
        super(description);
        this.amount = amount;
        this.required = required;
    }

    /**
     * Prints the leaf. First indentation, then put out the properties
     * @param indentation - number of tab steps
     */
    @Override
    public void print(int indentation) {
        for (int i = 0; i < indentation; i++)
            System.out.print("-");
        System.out.println(this);
    }

    @Override
    public String toString() {
        String prefix = required ? "(!)" : "( )";
        return prefix + description + ": " + NumberFormat.getCurrencyInstance().format(amount);
    }
}
