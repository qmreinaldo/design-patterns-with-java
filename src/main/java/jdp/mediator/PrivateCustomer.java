package jdp.mediator;

public class PrivateCustomer implements Consumer {
    private final String name;

    // Reference to the mediator
    private Mediator mediator;

    public PrivateCustomer(String name) {
        this.name = name;
    }

    /**
     *
     * @param quantity of wine to be requested at the mediator
     * @return cheapest offer
     */

    @Override
    public double requestPrice(int quantity) {
        System.out.println(name + " requests " + quantity
                        + " bottles of wine.");
        var totalPrice = mediator.getQuote(quantity);
        return totalPrice;
    }

    /**
     *
     * @param mediator to register to.
     */
    @Override
    public void register(Mediator mediator) {
        mediator.addConsumer(this);
        this.mediator = mediator;
    }
}
