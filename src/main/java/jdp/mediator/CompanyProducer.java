package jdp.mediator;

public class CompanyProducer implements Producer {
    private Mediator mediator;
    private final String name;

    public CompanyProducer(String name) {
        this.name = name;
    }

    /**
     * Gets called, when the mediator initiates a request
     * @param quantity - required
     * @return total price
     */
    @Override
    public double getQuote(int quantity) {
        var discountFactor = 1.0;
        if (quantity > 100)
            discountFactor = 0.7;
        else if (quantity > 50)
            discountFactor = 0.8;
        else
            discountFactor = 0.9;
        double price = Math.random()*9 + 1;
        price *= discountFactor;
        System.out.println("Producer " + name + " asks " + price + " per bottle.");
        return price*quantity;
    }

    /**
     * Producer registers itself to the mediator
     * @param mediator
     */
    @Override
    public void register(Mediator mediator) {
        mediator.addProducer(this);
        this.mediator = mediator;
    }
}
