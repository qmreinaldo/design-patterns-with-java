package jdp.mediator;

public interface Consumer {
    /**
     * Inquires the price for a certain amount of wine at the mediator.
     * The answer will be the price.
     * @param quantity of wine
     * @return price for this quantity
     */
    double requestPrice(int quantity);


    /**
     * Registers at the mediator
     * @param mediator
     */
    void register(Mediator mediator);
}
