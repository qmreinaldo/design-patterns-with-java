package jdp.mediator;

public class Example {
    public static void main(String[] args) {
        // Create a wholesale
        Wholesale wholesale = new Wholesale();

        // Create two customers
        var customer1 = new PrivateCustomer("Mario");
        var customer2 = new PrivateCustomer("Luigi");

        // Creates three suppliers
        var supplier1 = new CompanyProducer("Vineyard1");
        var supplier2 = new CompanyProducer("Vineyard2");
        var supplier3 = new CompanyProducer("Vineyard3");

        // Register consumers and producers to the mediator.
        customer1.register(wholesale);
        customer2.register(wholesale);
        supplier1.register(wholesale);
        supplier2.register(wholesale);
        supplier3.register(wholesale);

        // Generate inquiries and obtain quotations
        double price1 = customer1.requestPrice(50);
        double price2 = customer2.requestPrice(10);

    }
}
