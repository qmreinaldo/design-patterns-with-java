package jdp.mediator;

public interface Mediator {
    /**
     * Gets an inquiry from the consumer
     * @param quantity - liters or bottles
     * @return the best offer
     */
    double getQuote(int quantity);

    /**
     * Register new supplier
     * @param producer
     */
    void addProducer(Producer producer);

    /**
     * Deregister a supplier
     * @param producer
     */
    void removeProducer(Producer producer);

    /**
     * Register a new consumer
     * @param consumer
     */
    void addConsumer(Consumer consumer);

    /**
     * Deregister a new consumer
     * @param consumer
     */
    void removeConsumer(Consumer consumer);
}
