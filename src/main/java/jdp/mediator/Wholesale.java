package jdp.mediator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class Wholesale implements Mediator {
    private final List<Producer> producerList = new ArrayList<>();
    private final List<Consumer> consumerList = new ArrayList<>();

    /**
     * Gets called on requested by a consumer.
     * @param quantity - requested
     * @return total price
     */
    @Override
    public double getQuote(int quantity) {
        if (producerList.isEmpty())
            throw new IllegalStateException("There is no producers");
        List<Double> quotes = new ArrayList<>();

        for (Producer producer : producerList) {
            double quote = producer.getQuote(quantity);
            quotes.add(quote);
        }

        Double price = Collections.min(quotes);
        System.out.println("The best offer for " + quantity + " bottle is: " +
                price);
        return price;
    }

    /**
     *
     * @param producer - to add to the list
     */
    @Override
    public void addProducer(Producer producer) {
        producerList.add(producer);
    }

    /**
     *
     * @param producer - to remove from the list
     */
    @Override
    public void removeProducer(Producer producer) {
        producerList.remove(producer);
    }

    /**
     *
     * @param consumer - to add to the list
     */
    @Override
    public void addConsumer(Consumer consumer) {
        consumerList.add(consumer);
    }

    /**
     *
     * @param consumer - to add to the list
     */
    @Override
    public void removeConsumer(Consumer consumer) {
        consumerList.remove(consumer);
    }
}
