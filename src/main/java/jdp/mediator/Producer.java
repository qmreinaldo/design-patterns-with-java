package jdp.mediator;

public interface Producer {
    /**
     * Inquire the price for the requested quantity at the producer
     * @param quantity - requested
     * @return total price
     */
    double getQuote(int quantity);

    /**
     * Register a producer at the mediator.
     * @param mediator
     */
    void register(Mediator mediator);
}
