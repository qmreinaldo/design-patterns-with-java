package jdp.factorymethod.meal;

public class Example {
    public static void main(String[] args) {
        Restaurant mammaMia = new Pizzeria();
        mammaMia.order();

        Restaurant burgerQueen = new BurgerMaker();
        burgerQueen.order();
    }
}
