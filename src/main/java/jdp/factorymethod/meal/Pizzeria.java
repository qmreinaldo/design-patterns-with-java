package jdp.factorymethod.meal;

import javax.swing.*;

public class Pizzeria extends Restaurant {

    /**
     * Ask for the choice of the guest
     * @return the order
     */
    @Override
    protected String takeOrder() {
        String order = JOptionPane.showInputDialog("Have you already chosen your pizza?");
        return order;
    }

    /**
     * Make a new pizza
     * @return the pizza
     */
    @Override
    protected Meal prepareMeal() {
        return new Pizza();
    }
}
