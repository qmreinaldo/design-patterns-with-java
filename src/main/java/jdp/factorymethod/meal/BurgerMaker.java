package jdp.factorymethod.meal;

import javax.swing.*;

public class BurgerMaker extends Restaurant {

    /**
     * Ask for the ingredients
     * @return the order
     */
    @Override
    protected String takeOrder() {
        String order = JOptionPane.showInputDialog("Which ingredients do you want for your burger?");
        return order;
    }

    @Override
    protected Meal prepareMeal() {
        return new Burger();
    }
}
