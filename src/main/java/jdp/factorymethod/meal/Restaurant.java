package jdp.factorymethod.meal;

public abstract class Restaurant {
    /**
     * Ask the guest
     * @return the order
     */
    protected abstract String takeOrder();

    /**
     * Prepare the meal
     * @return the meal
     */
    protected abstract Meal prepareMeal();

    /**
     * Hand the meal out to the guest
     * @param meal
     */
    private void serveMeal(Meal meal) {
        System.out.println("Meal served. It is a " + meal);
    }

    public final Meal order() {
        takeOrder();
        Meal meal = prepareMeal();
        serveMeal(meal);
        return meal;
    }
}
