package jdp.factorymethod.meal;

public class Burger implements Meal{
    @Override
    public String toString() {
        return "burger";
    }
}
