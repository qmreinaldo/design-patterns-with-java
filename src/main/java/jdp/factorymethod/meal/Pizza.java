package jdp.factorymethod.meal;

public class Pizza implements Meal {
    @Override
    public String toString() {
        return "pizza";
    }
}
