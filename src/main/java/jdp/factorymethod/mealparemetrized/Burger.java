package jdp.factorymethod.mealparemetrized;

import javax.swing.*;

public class Burger implements Meal {
    private final String ingredient;

    Burger() {
        ingredient = null;
    }

    Burger(String ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public String toString() {
        String description = ingredient != null ? ingredient : "complete";
        return "Burger" + description;
    }
}
