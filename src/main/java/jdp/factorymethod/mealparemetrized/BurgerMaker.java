package jdp.factorymethod.mealparemetrized;

import javax.swing.*;

public class BurgerMaker extends Restaurant {

    /**
     * Ask for the ingredients
     * @return the order
     */
    @Override
    protected String takeOrder() {
        String order = JOptionPane.showInputDialog("Which ingredients do you want for your burger?");
        return order;
    }

    @Override
    protected Meal prepareMeal(String order) {
        return order.isEmpty() ? new Burger() : new Burger(order);
    }
}
