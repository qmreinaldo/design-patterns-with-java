package jdp.factorymethod.mealparemetrized;

public abstract class Restaurant {
    /**
     * Ask the guest
     * @return the order
     */
    protected abstract String takeOrder();

    /**
     * Prepare the meal
     * @param order of the guest
     * @return the meal
     */
    protected abstract Meal prepareMeal(String order);

    /**
     * Hand the meal out to the guest
     * @param meal
     */
    private void serveMeal(Meal meal) {
        System.out.println("Meal served. It is a " + meal);
    }

    public final Meal order() {
        String order = takeOrder();
        Meal meal = prepareMeal(order);
        serveMeal(meal);
        return meal;
    }
}
