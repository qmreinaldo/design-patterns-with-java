package jdp.factorymethod.mealparemetrized;

public class PizzaVegan extends Pizza {
    @Override
    public String toString() {
        return "A pizza with vegan ingredients";
    }
}
