package jdp.factorymethod.mealparemetrized;

public class PizzaHawaiian extends Pizza {
    @Override
    public String toString() {
        return "A pizza with pineapple";
    }
}
