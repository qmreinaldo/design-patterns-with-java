package jdp.factorymethod.mealparemetrized;

public class Pizza implements Meal {
    @Override
    public String toString() {
        return "pizza";
    }
}
