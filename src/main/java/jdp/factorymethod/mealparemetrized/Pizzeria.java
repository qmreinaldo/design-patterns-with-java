package jdp.factorymethod.mealparemetrized;

import javax.swing.*;

public class Pizzeria extends Restaurant {

    /**
     * Ask for the choice of the guest
     * @return the order
     */
    @Override
    protected String takeOrder() {
        String order = JOptionPane.showInputDialog("Have you already chosen your pizza?");
        return order;
    }

    /**
     * Make a new pizza
     * @return the pizza
     */
    @Override
    protected Meal prepareMeal(String order) {
        if (order == null || order.isEmpty())
            return new Pizza();
        else
            return switch (order) {
                case "hawaiian" -> new PizzaHawaiian();
                case "vegan" -> new PizzaVegan();
                default -> {
                    System.out.println("We don't offer this pizza.");
                    yield null;
                }
            };
    }
}
