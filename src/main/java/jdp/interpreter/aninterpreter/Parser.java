package jdp.interpreter.aninterpreter;

import jdp.interpreter.aninterpreter.expressions.*;
import jdp.interpreter.aninterpreter.symbols.EndSymbol;
import jdp.interpreter.aninterpreter.symbols.Symbol;


import java.util.Iterator;
import java.util.List;

public class Parser {
    /*
    An iterator on the list of symbols
     */
    private final Iterator<Symbol> iterator;

    /*
     The root of the abstract syntax tree
     */
    private final Expression root;

    /*
    The current symbol in processing
     */
    private Symbol currentSymbol;

    /**
     * Constructor: Assign the iterator and starts parsing the given expression
     * @param symbols- the list of symbols out of the scanner
     */
    public Parser(List<Symbol> symbols) {
        iterator = symbols.iterator();
        nextSymbol();
        root = parseExpression();
    }

    /**
     * Sets the current symbol the next one in the list.
     * If the list has ended, an EndSymbol is set.
     */
    private void nextSymbol() {
        currentSymbol = iterator.hasNext() ? iterator.next() : new EndSymbol();
    }

    /**
     * Starts parsing an expression
     * @return the parsed expression
     */
    private Expression parseExpression() {
        Expression expression = this.parseAddSub();
        return expression;
    }

    /**
     * Takes care of addition and subtraction.
     * According to the operator precedence, first takes look at multiplication and division.
     * @return the expression
     */
    private Expression parseAddSub() {
        Expression expression = this.parseMultiplicationDivision();
        while (currentSymbol.isPlus() || currentSymbol.isMinus()) {
            boolean isAddition = currentSymbol.isPlus();
            nextSymbol();
            Expression right = this.parseMultiplicationDivision();
            expression = isAddition ? new PlusExpression(expression, right)
                             : new MinusExpression(expression, right);
        }

        return expression;
    }

    /**
     * Takes care of multiplication and division.
     * According to operator precedence, first takes a look at unary leading signs.
     * @return the expression
     */

    private Expression parseMultiplicationDivision() {
        Expression expression = this.parseLeadingSign();
        while (currentSymbol.isMultiplication() || currentSymbol.isDivision()) {
            boolean isMultiplication = currentSymbol.isMultiplication();
            nextSymbol();
            Expression right = this.parseLeadingSign();
            expression = isMultiplication
                    ? new MultExpression(expression, right)
                    : new DivExpression(expression, right);
        }
        return expression;
    }

    /**
     * Parses a unary leading sign expression
     * @return
     */
    private Expression parseLeadingSign() {
        Expression expression;
        boolean isMinus = currentSymbol.isMinus();
        if (currentSymbol.isMinus() || currentSymbol.isPlus())
            nextSymbol();
        if (currentSymbol.isNumeral()) {
            expression = this.parseNumber();
        }
        else
            expression = parseParenthesis();

        if (isMinus)
            expression = new UnaryExpression(expression);
        return expression;
    }

    /**
     * Parses parentheses
     * @return the expression
     */
    private Expression parseParenthesis() {
        Expression expression;
        if (currentSymbol.isOpeningParenthesis()) {
            nextSymbol();
            expression = this.parseExpression();
            if (!currentSymbol.isClosingParenthesis())
                throw new RuntimeException("Closing parenthesis missing!");
            nextSymbol();
        }
        else
            throw new RuntimeException("Opening parenthesis missing");
        return expression;
    }

    /**
     * Parses and converts a number
     * @return the terminal expression
     */
    private Expression parseNumber() {
        if (!currentSymbol.isNumeral())
            throw new IllegalStateException("Current symbol is not a number");
        Double value = Double.parseDouble(currentSymbol.toString());
        nextSymbol();
        return new TerminalExpression(value);
    }

    /**
     * The start of the expression
     * @return the start
     */
    public Expression getRoot() {
        return root;
    }
}
