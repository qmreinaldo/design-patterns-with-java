package jdp.interpreter.aninterpreter.symbols;

public class RightParenthesis extends Symbol{
    @Override
    public boolean isClosingParenthesis() {
        return true;
    }

    @Override
    public String toString() {
        return " ) ";
    }
}
