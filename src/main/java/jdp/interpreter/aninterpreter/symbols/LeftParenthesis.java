package jdp.interpreter.aninterpreter.symbols;

public class LeftParenthesis extends Symbol {
    @Override
    public boolean isOpeningParenthesis() {
        return true;
    }

    @Override
    public String toString() {
        return " ( ";
    }
}
