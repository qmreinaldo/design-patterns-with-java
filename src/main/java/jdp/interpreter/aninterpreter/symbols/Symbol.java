package jdp.interpreter.aninterpreter.symbols;

public abstract class Symbol {
    /**
     * Check if it is a literal symbol
     * @return false by default
     */
    public boolean isLiteral() {
        return false;
    }

    /**
     * Check if it is numeral symbol
     * @return false by default
     */
    public boolean isNumeral() {
        return false;
    }

    /**
     * Check if it is a plus symbol
     * @return false by default
     */
    public boolean isPlus() {
        return false;
    }

    /**
     * Check if it is a minus symbol
     * @return false by default
     */
    public boolean isMinus() {
        return false;
    }

    /**
     * Check if it is a multiplication symbol
     * @return false by default
     */
    public boolean isMultiplication() {
        return false;
    }

    /**
     * Check if it is a division symbol
     * @return false by default
     */
    public boolean isDivision() {
        return false;
    }

    /**
     * Check if it is an equal symbol
     * @return false by default
     */
    public boolean isEquals() {
        return false;
    }

    /**
     * Check if it is a '(' symbol
     * @return false by default
     */
    public boolean isOpeningParenthesis() {
        return false;
    }

    /**
     * Check if it is a ')' symbol
     * @return false by default
     */
    public boolean isClosingParenthesis() {
        return false;
    }
}
