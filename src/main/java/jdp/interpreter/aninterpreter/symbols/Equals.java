package jdp.interpreter.aninterpreter.symbols;

public class Equals extends Symbol {
    @Override
    public boolean isEquals() {
        return true;
    }

    @Override
    public String toString() {
        return " = ";
    }
}
