package jdp.interpreter.aninterpreter.symbols;

public class Subtract extends Symbol {
    @Override
    public boolean isMinus() {
        return true;
    }

    @Override
    public String toString() {
        return " - ";
    }
}
