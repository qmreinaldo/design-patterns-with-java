package jdp.interpreter.aninterpreter.symbols;

import jdk.jshell.EvalException;

public class Numeral extends Symbol {
    /*
    The value of the numeral expression
     */
    final double value;

    /**
     * Constructor gets the value as a string
     * @param number - text of the numeral
     */
    public Numeral(String number) {
        this.value = Double.valueOf(number);
    }

    /**
     * Constructor gets the value as a number
     * @param number - the value of the number
     */
    public Numeral(double number) {
        this.value = number;
    }

    @Override
    public boolean isNumeral() {
        return true;
    }

    @Override
    public String toString() {
        return Double.toString(value);
    }
}
