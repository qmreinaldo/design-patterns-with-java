package jdp.interpreter.aninterpreter.symbols;

public class Divide extends Symbol {
    @Override
    public boolean isDivision() {
        return true;
    }

    @Override
    public String toString() {
        return " / ";
    }
}
