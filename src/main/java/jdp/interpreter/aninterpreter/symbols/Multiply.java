package jdp.interpreter.aninterpreter.symbols;

public class Multiply extends Symbol {
    @Override
    public boolean isMultiplication() {
        return true;
    }

    @Override
    public String toString() {
        return " * ";
    }
}
