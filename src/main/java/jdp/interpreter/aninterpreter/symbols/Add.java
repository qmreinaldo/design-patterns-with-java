package jdp.interpreter.aninterpreter.symbols;

public class Add extends Symbol {
    @Override
    public boolean isPlus() {
        return true;
    }

    @Override
    public String toString() {
        return " + ";
    }
}
