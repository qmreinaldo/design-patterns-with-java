package jdp.interpreter.aninterpreter.symbols;

public class Literal extends Symbol {
    /*
    The value of the literal
     */
    final char value;

    public Literal(Character character) {
        this.value = character;
    }

    @Override
    public boolean isLiteral() {
        return true;
    }

    @Override
    public String toString() {
        return Character.toString(value);
    }
}
