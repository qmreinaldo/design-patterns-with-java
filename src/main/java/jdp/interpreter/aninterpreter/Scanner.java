package jdp.interpreter.aninterpreter;

import jdp.interpreter.aninterpreter.symbols.*;

import java.util.*;

public class Scanner {
    /*
    List of scanned symbols
     */
    private final List<Symbol> symbols = new ArrayList<>();

    /*
    A hashmap of the valid operator symbols
     */
    private final Map<Character, Symbol> operators = new HashMap<>();

    /**
     * Constructor: Initializes the operators hashmap
     */
    private Scanner() {
        operators.put('+', new Add());
        operators.put('-', new Subtract());
        operators.put('*', new Multiply());
        operators.put('/', new Divide());
        operators.put('(', new LeftParenthesis());
        operators.put(')', new RightParenthesis());
        operators.put('=', new Equals());
    }

    /**
     * Constructor: Splits the input into a list of its symbols
     * @param input- the entered text
     * @throws IllegalArgumentException
     */
    public Scanner(String input) throws IllegalArgumentException {
        this();

        // First, reduce all unnecessary spaces
        input = input.replaceAll(" ", "");

        int length = input.length();

        // Look for a semicolon
        int firstSemicolon = input.indexOf(';');

        // Semicolon should be at the last position of the input
        if (firstSemicolon != length-1)
            throw new IllegalArgumentException("You must end your input with a semicolon. ");

        // Take a look at the last character of the input
        char lastCharacter = input.charAt(length-1);

        // Last character should be a semicolon
        if (lastCharacter != ';')
            throw new IllegalArgumentException("The last character must be a semicolon.");

        for (int i = 0; i < input.length(); i++) {
            Character character = input.charAt(i);

            // Only step further if it is not the end
            if (character.equals(';'))
                break;

            // Check it the character is an operator or a literal
            if (operators.containsKey(character)) {

                symbols.add(operators.get(character));
            }

            else if (Character.isLetter(character))
                symbols.add(new Literal(character));
            else {
                // Build a number by scanning all following digits
                StringBuilder numberBuilder = new StringBuilder();
                while (Character.isDigit(character)) {
                    numberBuilder.append(character);
                    character = input.charAt(++i);
                }

                if (character == '.' || character == ',') {
                    character = '.';
                    numberBuilder.append(character);
                    i++;
                    character = input.charAt(i);
                    while (Character.isDigit(character)) {
                        numberBuilder.append(character);
                        character = input.charAt(++i);
                    }
                }

                symbols.add(new Numeral(numberBuilder.toString()));
                i--;
            }
        }

    }

    /**
     * Get all the scanned symbols
     * @return an unmodifiable list of symbols
     */
    public List<Symbol> getSymbols() {
        return Collections.unmodifiableList(symbols);
    }

    /**
     * Get all the scanned symbols
     * @return the list converted into a string
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Symbol symbol : symbols)
            builder.append(symbol.toString());
        return builder.toString();
    }
}
