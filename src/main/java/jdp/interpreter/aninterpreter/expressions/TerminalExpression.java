package jdp.interpreter.aninterpreter.expressions;

public class TerminalExpression implements Expression {
    /*
    The current value
     */
    private final double value;

    /**
     * Constructor: Gets the value
     * @param value - of the expression
     */
    public TerminalExpression(double value) {
        this.value = value;
    }

    /**
     * Just return the value itself
     * @return the result
     */

    @Override
    public double calculate() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Terminal ( " + Double.toString(value) + " )";
    }
}
