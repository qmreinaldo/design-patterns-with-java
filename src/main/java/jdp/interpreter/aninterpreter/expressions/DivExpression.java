package jdp.interpreter.aninterpreter.expressions;

public class DivExpression extends NonTerminalExpression {
    /**
     * Constructor: Gets the two sub expressions
     * @param left - the left part
     * @param right - the right part
     */
    public DivExpression(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Divide the two expressions
     * @return the division
     */
    @Override
    public double calculate() {
        return left.calculate() / right.calculate();
    }

    @Override
    public String toString() {
        return "DivExpression (" + left + " " + right + " )";
    }
}
