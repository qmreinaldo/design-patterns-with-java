package jdp.interpreter.aninterpreter.expressions;

public abstract class NonTerminalExpression implements Expression {
    /*
    The left part of an expression is also an expression
     */
    protected final Expression left;

    /*
    The right part of an expression is also an expression
     */
    protected final Expression right;

    /**
     * Constructor: Gets the two sub expressions for the left and right side
     * @param left - left part
     * @param right - right part
     */
    protected NonTerminalExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }
}
