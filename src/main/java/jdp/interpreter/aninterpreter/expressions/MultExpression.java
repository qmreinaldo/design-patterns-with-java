package jdp.interpreter.aninterpreter.expressions;

public class MultExpression extends NonTerminalExpression{
    /**
     * Constructor: Gets the two sub expressions
     * @param left - the left part
     * @param right - the right part
     */
    public MultExpression(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Multiply the two expressions
     * @return the product
     */
    @Override
    public double calculate() {
        return left.calculate() * right.calculate();
    }

    @Override
    public String toString() {
        return "MultExpression (" + left + " " + right + " )";
    }
}
