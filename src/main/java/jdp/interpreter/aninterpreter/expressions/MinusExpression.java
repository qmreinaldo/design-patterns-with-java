package jdp.interpreter.aninterpreter.expressions;

public class MinusExpression extends NonTerminalExpression {

    /**
     * Constructor: Gets the two sub expressions
     *
     * @param left  - left part
     * @param right - right part
     */
    public MinusExpression(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Subtract the two expressions
     * @return the difference
     */
    @Override
    public double calculate() {
        return left.calculate() - right.calculate();
    }

    @Override
    public String toString() {
        return "MinusExpression ( " + left + " " + right + " )";
    }
}
