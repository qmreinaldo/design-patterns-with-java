package jdp.interpreter.aninterpreter.expressions;

public class PlusExpression extends NonTerminalExpression {

    /**
     * Constructor: Gets the two sub expressions
     * @param left - the left part
     * @param right - the right part
     */
    public PlusExpression(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Adds two expressions
     * @return the sum
     */
    @Override
    public double calculate() {
        return left.calculate() + right.calculate();
    }

    @Override
    public String toString() {
        return "PlusExpression (" + left + " " + right + " )";
    }
}
