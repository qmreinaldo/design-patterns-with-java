package jdp.interpreter.aninterpreter.expressions;

public interface Expression {

    /**
     *
     * @return result of the calculation
     */
    double calculate();
}
