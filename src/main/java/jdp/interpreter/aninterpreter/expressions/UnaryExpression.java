package jdp.interpreter.aninterpreter.expressions;

public class UnaryExpression implements Expression {
    /*
    The current expression
     */
    private final Expression expression;

    /**
     * Constructor of a unary expression
     * @param expression
     */
    public UnaryExpression(Expression expression) {
        this.expression = expression;
    }

    /**
     * Evaluates the unary (minus) expression
     * @return the result
     */

    @Override
    public double calculate() {
        return -(expression.calculate());
    }

    @Override
    public String toString() {
        return "LeadingMinus ( " + expression + " )";
    }
}
