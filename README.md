# Singleton

**Purpose:** Ensure that a class has exactly one copy, and provide a global access point to it. 

**Implementation:** You should ensure that there is only one copy of the class setting the constructor to private. Define a public method that creates an instance if there is no instance already and return the instance. Keep a reference to this instance in a static variable. 

```java
public class MySingleton {
    
    private static MySingleton instance;

    private MySingleton() { }

    public static MySingleton getInstance() {
        if (instance == null)   // Lazy initialization
            return new MySingleton();
        return instance;
    }
}
```

The above code works if you don’t use concurrency. In such case you can use synchronization. 

Instead of locking the entire method, you can lock only the critical part. 

```java
public class MySingleton {
    // ...
    public static MySingleton getInstance() {
        if (instance == null) {
            synchronized (MySingleton.class) {
                if (instance == null)   // thread-safe query
                    instance = new MySingleton();
            }
        }
        return instance;
    }
	//...
}
```

You can also prevent all the problems in concurrent systems using early loading, that is, you initialize the static variable that holds the reference to the single instance as soon as you load it. 

```java
public class MySingleton {
    private static final MySingleton INSTANCE = new MySingleton();  // Early loading.

    private MySingleton() { }

    public static MySingleton getInstance() {
        return INSTANCE;
    }
}
```

Disadvantages of the Singleton design pattern:

- Violates the SRP since the class must take care of both business logic and its own object creation.
- Whether a class makes use of a singleton class cannot be seen form the interface, but only from the code.
- You can’t create subclasses of singleton classes.
- Singletons create something like a global state. This makes testing harder.

# Template Method

**Purpose:** Define the skeleton of an algorithm in an operation and delegate individual steps to subclasses. Using a template method allows subclasses to override specific steps of an algorithm without changing its structure. 

**Implementation.** Build an abstract superclass that defines the algorithm and the methods that are identical in all subclasses. The algorithm should be declared final since its definition should be binding for all subclasses. 

In the following example, we want to input a string, convert it optionally to upper or lower case, and output it to the console. 

```java
**public abstract class Input {

    /* Use final to make the algorithm bind all subclasses */
    public final void run() {
        var input = textEnter();
        var converted = convert(input);
        print(converted);
    }
    private final String textEnter() {
        return JOptionPane.showInputDialog("Please enter the text: ");
    }

    /* This method should be overridden in the subclasses. */
    protected abstract String convert(String input);

    private final void print (String text) {
        System.out.println(text);
    }
}**
```

```java
public class LowercaseConverter extends Input {

    @Override
    protected String convert(String input) {
        return input.toLowerCase();
    }
}
```

```java
public class UppercaseConverter extends Input {

    @Override
    protected String convert(String input) {
        return input.toUpperCase();
    }
}
```

The Hollywood principle: Don’t call us, we’ll call you!

```java
public class Client {
    public static void main(String[] args) {
        Input input =  new LowercaseConverter();
        input.run(); // The call is always made from the superclass. 

        Input anotherInput = new UppercaseConverter();
        anotherInput.run();
    }
}
```

We can extend the above example introducing **hook methods**. A hook method is a method that can optionally be overridden. A default behavior (if any) is defined in the superclass. The subclass can (but do not have to) adapt this behavior. 

The `save()` method returns if the entered text should be saved to disk.  By default false is returned.

```java
public abstract class Input {
    public final void run() {
        var input = textEnter();
        var converted = convert(input);
        print(converted);
        if (save())
            saveToDisk();
    }

    // ...

    /* hook method */
    protected boolean save() {
        return false; // by default
    }

    private void saveToDisk() {
        System.out.println("Input saved");
    }
}
```

```java
public class UppercaseConverter extends Input {

    // ...

    @Override
    protected boolean save() {
        return true;
    }
}
```

```java
public class LowercaseConverter extends Input {

    // ...

    @Override
    protected boolean save() {
        var question = "Should the text be saved?";
        var answer = JOptionPane.showConfirmDialog(null, question);
        return answer == JOptionPane.YES_OPTION;
    }
}
```
